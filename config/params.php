<?php

return [
    'adminEmail' => 'admin@example.com',

    'userRole' => [
        'admin' => 'admin',
        'investor' => 'investor',
        'moder' => 'operator',
        'operatorWork' => 'operatorWork',
        'operatorMaterial' => 'operatorMaterial',
    ],
];
