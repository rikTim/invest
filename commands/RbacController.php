<?php

namespace app\commands;

use yii\console\Controller;
use Yii;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;


        /** создание роли админа */
        $admin = $auth->createRole('admin');
        $auth->add($admin);


        /** создание роли оператор чата */
        $chatOperator = $auth->createRole('moder');
        $auth->add($chatOperator);

        /** создание роли оператор сендер */
        $senderOperator = $auth->createRole('investor');
        $auth->add($senderOperator);


        $operatorMaterial = $auth->createRole('operatorMaterial');
        $auth->add($operatorMaterial);

        $operatorWork =  $auth->createRole('operatorWork');
        $auth->add($operatorWork);

        /** Делаем пользователя с id = 1 админом */
        $auth->assign($admin, 1);
    }
}
