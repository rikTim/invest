<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\SenderSql;
use app\models\UsdKurs;
use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";
    }
//    public function actionSenderBase()
//    {
//
//        $numbers = [
//            '+380969597927'
//        ];
//        $server = '136.243.146.111';
//        $user = 'sen_asia_rus';
//        $password = 'I2sKG1N6';
//        $dblink = mysql_connect($server, $user, $password);
//        $msg = '';
//        if ($dblink) {
//        } else {
//            $msg = $msg . 'Ошибка подключения к серверу баз данных.';
//        }
//        $database = 'sen_asia_rus';
//        $selected = mysql_select_db($database, $dblink);
//        if ($selected) {
//        } else
//            $msg = $msg . ' База данных не найдена или отсутствует доступ.';
//        if (!empty($msg)) {
//            $sms = new \SoapClient('http://vipsms.net/api/soap.html');
//            $res = $sms->auth('agencyodessa', 'qazqaz');
//            if ($res->code == 0) {
//                $sessId = $res->message;
//                foreach ($numbers as $key => $value) {
//                    $sms->sendSmsOne($sessId, $value, 'w229.net', $msg);
//                }
//            }
//        }
//    }


    public function actionUsd()
    {
        $url ='https://about.pumb.ua/ru/info/currency_converter';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_ENCODING, '');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE,'topdates.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR,'topdates.txt');
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0);

        $out = curl_exec($curl);
        curl_close($curl);
        $model  = new UsdKurs();

        $model->datetime = date('Y-m-d');
        if (preg_match_all('|<tr style="border-bottom: 1px solid #2d2f32;">\r?\n.+<td>USD</td>\r?\n.+<td>.*</td>\r?\n.+<td>(.*)</td>|m', $out, $m)) {
            $model->kurs = $m[1][0];
        }
        $model->save();


    }


    public function actionSenderBase()
    {
        $server = '136.243.146.111';
        $user = 'sen_asia_rus';
        $password = 'I2sKG1N6';
        $dblink = mysql_connect($server, $user, $password);
        $database = 'sen_asia_rus';
        $selected = mysql_select_db($database, $dblink);
        if ($selected) {
            $sql = $sql = 'INSERT INTO sender_sql (timestamp, datetime, send_message) VALUES (' . time() . ',"' . date("Y-m-d H:i:s") . '" ,0 )';
            \Yii::$app->db->createCommand($sql)->execute();
        }

    }


    public function actionSendMessage()
    {
        $numbers = [
            '+380969597927',
            '+380935047360'
        ];

        $model = new SenderSql();
        $time = time() - 240;
        $columns = $model->find()->where(' timestamp > ' . $time . '')->one();
        if (empty($columns)) {
            $time = time() - 1800;
            $send_message = $model->find()->where(' timestamp > ' . $time . ' AND send_message = 1')->one();
            if (empty($send_message)) {
                $sms = new \SoapClient('http://vipsms.net/api/soap.html');
                $res = $sms->auth('agencyodessa', 'qazqaz');
                if ($res->code == 0) {
                    $sessId = $res->message;
                    foreach ($numbers as $key => $value) {
                        $sms->sendSmsOne($sessId, $value, 'w229.net', "База данных Sender не работает");
                    }
                }
                $sql = $sql = 'INSERT INTO sender_sql (timestamp, datetime, send_message) VALUES (' . time() . ',"' . date("Y-m-d H:i:s") . '",1 )';
                \Yii::$app->db->createCommand($sql)->execute();
                $this->senderBaseUp();
            }
        }

    }


    public function senderBaseUp()
    {
        $con = ssh2_connect('136.243.146.111', 22);
        if ($con) {
            ssh2_auth_password($con, 'root', 'XQ32bprqbGjgis');
            ssh2_exec($con, 'service mysql start');
        }

    }



    public function actionSendParis()
    {
        $numbers = [
//            ['name' => 'Дмитрий', 'phone' => '+33666061804'],
//            ['name' => 'Лия', 'phone' => '+330613905875'],
//            ['name' => 'Феликс', 'phone' => '33652066646'],
//            ['name' => 'Татьяна', 'phone' => '+33781268080'],
//            ['name' => 'Марина', 'phone' => '+33684894119'],
//            ['name' => 'Артур', 'phone' => '+33695147767'],
//            ['name' => 'Евгений', 'phone' => '+330667309606'],
//            ['name' => 'Марк', 'phone' => '+330612096358'],
//            ['name' => 'Нарек', 'phone' => '+33778843878'],
//            ['name' => 'Айк', 'phone' => '+33651522222'],
//            ['name' => 'Татьяна', 'phone' => '+33647389064'],
//            ['name' => 'Андрей', 'phone' => '+358923167011'],
//            ['name' => 'Полина', 'phone' => '+33698152910'],
//            ['name' => 'Наталья', 'phone' => '+3361745002'],
//            ['name' => 'Ольга', 'phone' => '+33678105391'],
//            ['name' => 'Вадим', 'phone' => '+330768477769'],
//            ['name' => 'Владимир', 'phone' => '+33698609615'],
//            ['name' => 'Елена', 'phone' => '+33753197816'],
//            ['name' => 'Артем', 'phone' => '+33652127988'],
//            ['name' => 'Дарья', 'phone' => '+33753262333'],
//            ['name' => 'Дарья', 'phone' => '+33681441332'],
//            ['name' => 'Юлия', 'phone' => '+33664552032'],
//            ['name' => 'МОльга', 'phone' => '+330662597807'],
//            ['name' => 'Игорь', 'phone' => '+330612687905'],
//            ['name' => 'Валерий', 'phone' => '+34660581178'],
//            ['name' => 'Игорь', 'phone' => '+420608362909'],
//            ['name' => 'Вадим', 'phone' => '+496221485426'],
//            ['name' => 'Елизавета', 'phone' => '+30632795277'],
//            ['name' => 'Юлия', 'phone' => '+33782831530'],
//            ['name' => 'Юлия', 'phone' => '+330647475495'],
//            ['name' => 'Ольга', 'phone' => '+33633707557'],
//            ['name' => 'Валерий', 'phone' => '+34676180420'],
//            ['name' => 'Сергей', 'phone' => '+33671753173'],
//            ['name' => 'Татьяна', 'phone' => '+330951823541'],
//            ['name' => 'Виктор', 'phone' => '+33662210817'],
            ['name' => 'Николай', 'phone' => '+380969597927'],
        ];


//        if (!empty($send_message)) {
            $sms = new \SoapClient('http://vipsms.net/api/soap.html');
            $res = $sms->auth('agencyodessa', 'qazqaz');
            if ($res->code == 0) {
                $sessId = $res->message;
                foreach ($numbers as $key => $value) {

                    $send_message = "Здравствуйте ". $value['name'].", Ищу партнера в Париже для реализации рекламного проекта . Если Вам интересно пожалуйста напишите мне +4915778192087" ;
//                    print_r($send_message); echo ' '.$value['phone'].' ' ;
                    echo $key.' '.$value['phone'].' ';
                    $sms->sendSmsOne($sessId, $value['phone'], 'w229.net', $send_message);

                }
            }
        }


//    }
}