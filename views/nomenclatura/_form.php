<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;

/* @var $this yii\web\View */
/* @var $model app\models\Nomenclatura */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nomenclatura-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'provider')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'project_id')->hiddenInput(['value'=>$project_id])->label(false); ?>

    <?php if (Yii::$app->user->can('admin')): ?>
        <?= $form->field($model, 'status_id')->dropDownList($status, ['prompt' => 'Выберите статус']) ?>
    <?php else: ?>
        <?= $form->field($model, 'status_id')->hiddenInput(['value' => 1])->label(false); ?>
    <?php endif; ?>


    <?php if (!empty($model->datetime)) {
        $model->datetime = Yii::$app->formatter->asDatetime($model->datetime, "php:m/d/Y ");
    } else {
        $model->datetime = Yii::$app->formatter->asDatetime(time(), "php:m/d/Y ");
    } ?>
    <?php
    echo \yii\widgets\MaskedInput::widget([
        'model' => $model,
        'attribute' => 'datetime',
        'name' => 'datetime',
        'mask' => '99/99/9999'
    ]);
    ?>

    <?= $form->field($model, 'stage_id')->dropDownList(['3'=>'Ступень № 3,4  - Коробка']+$stage, ['prompt' => 'Выберите ступень']) ?>

    <?= $form->field($upload, 'imageFile')->fileInput()->label('Фото Стоимости') ?>

    <?= $form->field($file, 'files[]')->fileInput(['multiple' => true])->label('Документы') ?>

<!--    --><?//= $form->field($model, 'datetime')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
