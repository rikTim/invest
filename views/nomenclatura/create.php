<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Nomenclatura */

?>
<div class="nomenclatura-create">
    <?= $this->render('_form', [
        'model' => $model,
        'project_id'=>$project_id,
        'status' => $status,
        'upload' => $upload,
        'file' => $file,
        'stage'=> $stage,
    ]) ?>
</div>
