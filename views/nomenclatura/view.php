<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Nomenclatura */
?>
<div class="nomenclatura-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'provider',
            'project_id',
            'status_id',
            'datetime',
        ],
    ]) ?>

</div>
