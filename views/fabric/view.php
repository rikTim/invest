<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Fabric */

$this->title = 'View Fabric' . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Fabrics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fabric-view">

    <div class="box">

        <div class="box-header with-border">
            <?php if (Yii::$app->user->can('admin') || Yii::$app->user->can('moder')): ?>
        <?= Html::a('Назад', ['index','id'=>$model->project_id], ['class' => 'btn btn-warning']) ?>
        <?= Html::a('Создать', ['create','project_id'=>$model->project_id], ['class' => 'btn btn-success']) ?>
                <?php else:?>
                <?= Html::a('Назад', ['finance/material','id'=>$model->project_id], ['class' => 'btn btn-warning']) ?>
            <?php endif;?>
        </div>

        <div class="box-body">
            <?php $model->datetime = date('d.m.Y', $model->datetime)?>

            <?php $model->status_id = $model->status->name;?>
            <?php $model->material_id = $model->material->name;?>
            <?php $model->project_id = $model->project->name;?>
        <?= DetailView::widget([
        'model' => $model,
            'attributes' => [
            'datetime',
            'brigade',
            'status_id',
            'material_id',
            'money',
            'kurs',
            'project_id',
            ],
        ]) ?>
        </div>
        <div class="box-body">

            <?php if(!empty($model->money_picture) && isset($model->avatar->path)):?>
            <div id='gallery'>
                        <a href="<?= Yii::getAlias('@web') . '/images/fabric/' . $model->id. '/' . $model->avatar->path ?> "><img
                            style="width: 240px" class="news-img"
                            src="<?= Yii::getAlias('@web') . '/images/fabric/' . $model->id . '/cropped/' . $model->avatar->path ?>"></a>
            </div>
        </div>
        <?php endif;?>

        <?php if (!empty($files)): ?>
            <div class="box-body">
                <h3>Документы</h3>
                <?php foreach ($files as $count=> $file): ?>
                    <div>
                        <img  style="width: 40px" src="<?=Yii::getAlias('@web'). '/images/file.png '?>">
                        <a class="btn btn-default" href="<?= Yii::getAlias('@web') . '/images/file/' . $file->work_id . '/' . $file->path ?>" download="<?=  $file->path ?>"> Скачать файл_<?= $count?></a>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>

</div>
