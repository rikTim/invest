<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Fabric */

$this->title = 'Update Fabric' . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Fabrics', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="fabric-update">

    <?= $this->render('_form', [
        'model' => $model,
        'id' => $id,
        'status' => $status,
        'brigade' => $brigade,
        'material' => $material,
        'upload' => $upload,
//        'money' => $money,
        'file' => $file,
    ]) ?>

</div>
