<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FabricSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Fabrics';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fabric-index">

    <div class="box">

        <div class="box-header with-border">
            <div class="pull-left">
                <?= Html::a('Добавить материал', ['create','project_id'=>$id], ['class' => 'btn btn-success']) ?>
            </div>
            <div class="pull-right">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                    </div>
        </div>

        <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'id',
//            'datetime',
            [
                'attribute'=>'brigade',
                'label'=>'Бригада',

            ],
            [
                'attribute'=>'status.name',
                'label'=>'Статус',
                'format' => 'raw',
                'value' => function($data){

                    $status = $data->status->name;
                    switch ($status) {
                        case 'новый':
                            return '<span class="label bg-green "> ' . $data->status->name . '</span>';
                            break;
                        case 'подтвержденный':
                            return '<span class="label bg-blue "> ' . $data->status->name . '</span>';
                            break;
                        case 'законченый':
                            return '<span class="label bg-yellow "> ' . $data->status->name . '</span>';
                            break;
                        default:
                            return $data->status->name;
                            break;
                    }
                }

            ],
            [
                'attribute'=>'material.name',
                'label'=>'Материал',

            ],


            [
                'attribute' => 'money_picture',
                'format' => 'html',
                'label' => 'Фото',
                'value' => function ($data) {
                    if(!empty($data->money_picture)) {
                        return '<img src=' . Yii::getAlias('@web') . '/images/fabric/' . $data->id . '/cropped/' . $data->avatar->path . '>';}

                    else return false;
//
                }
            ],
//             'kurs',
            'money',
            [
                'attribute'=>'success',
                'label' => 'Оплачено',
            ],
            // 'project_id',

            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '70'],
                'template' => '{view} {update} {money}',
                'visibleButtons'=> [
                    'update'=>function ($model, $key, $index) {
                        return $model->status_id == 1;
                    },
                    'money'=> function ($model, $key, $index) {
                        return  $model->success < $model->money;
                    },
                ],
                'buttons' => [
                    'money' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-usd"></span>', $url, [
                            'title' => Yii::t('app', 'Work'),
                        ]);
                    },


                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    switch($action){
                        case 'money':
                            $url = \yii\helpers\Url::toRoute(['material-money/index','id'=>$model->id]);
                            return $url;
                        case 'update':
                            $url = \yii\helpers\Url::toRoute(['fabric/update','id'=>$model->id]);
                            return $url;
                        case 'view':
                            $url = \yii\helpers\Url::toRoute(['fabric/view','id'=>$model->id]);
                            return $url;


                    }


                }
            ],
        ],
    ]); ?>
        </div>

    </div> <!--end box -->

</div>
