<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Fabric */

$this->title = 'Create Fabric';
$this->params['breadcrumbs'][] = ['label' => 'Fabrics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fabric-create">

    <?= $this->render('_form', [
        'model' => $model,
        'id' => $id,
        'status' => $status,
        'brigade' => $brigade,
        'material' => $material,
        'upload' => $upload,
        'money' => $money,
        'file' => $file,
    ]) ?>

</div>
