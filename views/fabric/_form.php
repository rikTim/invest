<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Fabric */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fabric-form">

    <div class="box box-default">

        <div class="box-body">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'brigade')->textInput(['maxlength' => true]) ?>

            <?php if (Yii::$app->user->can('admin')): ?>
                <?= $form->field($model, 'status_id')->dropDownList($status) ?>
            <?php else: ?>
                <?= $form->field($model, 'status_id')->hiddenInput(['value' => 1])->label(false); ?>
            <?php endif; ?>

            <?= $form->field($model, 'material_id')->dropDownList($material) ?>
            <!--            --><? //= $form->field($model, 'kurs')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'price')->textInput(['maxlength' => true,'id'=>'price']) ?>

            <?= $form->field($model, 'number')->textInput(['maxlength' => true,'id'=>'number']) ?>


            <?= $form->field($model, 'money')->textInput(['maxlength' => true, 'id' => 'sum-money','disabled' => 'disabled']) ?>

            <?= $form->field($model, 'project_id')->hiddenInput(['value' => $id])->label(false) ?>

            <?= $form->field($upload, 'imageFile')->fileInput()->label('Фото Стоимости') ?>

            <?= $form->field($file, 'files[]')->fileInput(['multiple' => true])->label('Документы') ?>
            <br>
            <?php if ($model->isNewRecord): ?>
                <h3>Деньги</h3>
                <?= $form->field($money, 'money')->textInput(['maxlength' => true,]) ?>

                <?= $form->field($money, 'text')->textarea(['rows' => 6]) ?>
            <?php endif; ?>
        </div>

        <div class="box-footer">
            <div class="form-btn">
                <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
