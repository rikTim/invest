<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Stages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stage-index">

    <div class="box">

        <div class="box-header with-border">
            <div class="pull-left">
                <?= Html::a('Добавить этап', ['create'], ['class' => 'btn btn-success']) ?>
            </div>
            <div class="pull-right">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                    </div>
        </div>

        <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'name',

            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '70'],
                'template' => '{view} {update} ',
            ],
        ],
    ]); ?>
        </div>

    </div> <!--end box -->

</div>
