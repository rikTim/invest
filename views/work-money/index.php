<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\WorkMoneySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Work Moneys';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-money-index">

    <div class="box">

        <div class="box-header with-border">
            <div class="pull-left">
                <?= Html::a('Добавить виплату', ['create','id'=>$id], ['class' => 'btn btn-success']) ?>
            </div>
            <div class="pull-right">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                    </div>
        </div>

        <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'work.work.name',
            'money',
            'text:ntext',
            [
                'attribute' => 'datetime',
                'filter' => \yii\jui\DatePicker::widget(['model' => $searchModel, 'attribute' => 'datetime',
                    'language' => 'ru',
                    'dateFormat' => 'yyyy-MM-dd',
                    'options' => ['class' => 'form-control'],
                ]),
                'content' => function ($data) {
                    if (!empty($data->datetime)) {
                        return date('d.m.Y H:i:s', $data->datetime);
                    }
                }
            ],

//
//            [
//                'class' => 'yii\grid\ActionColumn',
//                'headerOptions' => ['width' => '70'],
//                'template' => '{view} {update} {delete} {link}',
//            ],
        ],
    ]); ?>
        </div>

    </div> <!--end box -->

</div>
