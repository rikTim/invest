<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\WorkMoney */

$this->title = 'Create Work Money';
$this->params['breadcrumbs'][] = ['label' => 'Work Moneys', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-money-create">

    <?= $this->render('_form', [
        'model' => $model,
        'id'=>$id,
        'project_id'=>$project_id,
        'user'=>$user,
    ]) ?>

</div>
