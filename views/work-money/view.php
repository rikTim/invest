<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\WorkMoney */

$this->title = 'View Work Money' . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Work Moneys', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-money-view">

    <div class="box">

        <div class="box-header with-border">
            <?php if(Yii::$app->user->can('admin')):?>
        <?= Html::a('Назад', ['index','id'=>$model->work_id], ['class' => 'btn btn-warning']) ?>
        <?= Html::a('Добавить', ['create','id'=>$model->work_id], ['class' => 'btn btn-success']) ?>
<!--        --><?//= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
<!--        --><?//= Html::a('Delete', ['delete', 'id' => $model->id], [
//            'class' => 'btn btn-danger',
//            'data' => [
//                'confirm' => 'Are you sure you want to delete this item?',
//                'method' => 'post',
//            ],
//        ]) ?>
            <?php else:?>
                <!--                --><?//= Html::a('Назад', ['investor-project/mai','id'=>$model->nomenclatura_id], ['class' => 'btn btn-warning']) ?>
            <?php endif?>
        </div>

        <div class="box-body">
        <?php
            $model->work_id = $model->work->work->name;
        $model->datetime = date('d.m.Y H:i:s', $model->datetime);
        ?>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
//                'id',
            'work_id',
            'money',
            'text:ntext',
            'datetime',
            ],
        ]) ?>

        </div>

    </div>

</div>
