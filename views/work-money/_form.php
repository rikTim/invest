<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\WorkMoney */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="work-money-form">

    <div class="box box-default">

        <div class="box-body">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'work_id')->hiddenInput(['value' => $id])->label(false) ?>

            <?= $form->field($model, 'money')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'investor_id')->dropDownList($user,['prompt' => 'Виберите инвестора']) ?>

<!--            --><?//= $form->field($model, 'kurs')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>
            <?= $form->field($model, 'project_id')->hiddenInput(['value' => $project_id])->label(false) ?>

            <!--    --><? //= $form->field($model, 'datetime')->textInput(['maxlength' => true]) ?>

        </div>

        <div class="box-footer">
            <div class="form-btn">
                <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
