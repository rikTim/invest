<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\InvestorMoney */

$this->title = 'Create Investor Money';
$this->params['breadcrumbs'][] = ['label' => 'Investor Moneys', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="investor-money-create">
    <div class="investor-money-form">

        <div class="box box-default">

            <div class="box-body">

                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'money_usd')->textInput(['maxlength' => true]) ?>


                <?= $form->field($model, 'kurs')->textInput(['maxlength' => true]) ?>

<!--                --><?//= $form->field($model, 'project_id')-> hiddenInput(['value'=>$project_id])->label(false) ?>
                <?php if(Yii::$app->user->can('admin')):?>
                <?= $form->field($model, 'investor_id')->dropDownList($user) ?>
                <?php else:?>
                    <?= $form->field($model, 'investor_id')->hiddenInput(['value'=>Yii::$app->user->id])->label(false) ?>
                <?php endif;?>
            </div>

            <div class="box-footer">
                <div class="form-btn">
                    <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

    </div>
</div>
