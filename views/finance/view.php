<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Work */

$this->title = 'View Work' . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Works', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-view">

    <div class="box">

        <div class="box-header with-border">
        <?= Html::a('назад', ['index','id'=>$project_id], ['class' => 'btn btn-warning']) ?>


        </div>

        <div class="box-body">

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [

            'datetime',
//            'operator_id',
            'status_id',
            'stage_id',
            'work_id',
            'note:ntext',
            'money',
//            'money_picture',
            'project_id',
            ],
        ]) ?>

            <?php if (!empty($model->money_picture)): ?>
                <div class="box-body">
                    <div id='gallery'>


                        <a href="<?= Yii::getAlias('@web') . '/images/work/' . $model->id . '/' . $model->avatar->path ?> "><img
                                style="width: 240px" class="news-img"
                                src="<?= Yii::getAlias('@web') . '/images/work/' . $model->id . '/cropped/' . $model->avatar->path ?>"></a>


                    </div>
                </div>
            <?php endif; ?>


        </div>

    </div>

</div>
