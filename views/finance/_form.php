<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Work */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="work-form">

    <div class="box box-default">

        <div class="box-body">

            <?php $form = ActiveForm::begin(); ?>


            <?php if(Yii::$app->user->can('admin')):?>
            <?= $form->field($model, 'status_id')->dropDownList($status) ?>
            <?php else:?>
                <?= $form->field($model, 'status_id')->hiddenInput(['value'=>1])->label(false); ?>
            <?php endif;?>
            <?= $form->field($model, 'stage_id')->dropDownList($stage, ['id'=>'cat-id','prompt' => 'Выберите ступень']) ?>

            <?= $form->field($model, 'work_id')->widget(\kartik\depdrop\DepDrop::classname(), [
                'options' => ['id' => 'subcat-id'],
                'pluginOptions' => [
                    'depends' => ['cat-id'],
                    'placeholder' => 'Выберите...',
                    'url' => \yii\helpers\Url::to(['/work/work'])
                ]
            ]); ?>

            <?= $form->field($model, 'note')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'money')->textInput(['maxlength' => true]) ?>


            <?= $form->field($upload, 'imageFile')->fileInput()->label(false) ?>

            <?= $form->field($model, 'project_id')->hiddenInput(['value' => $project_id])->label(false); ?>


            <?php if (!$model->isNewRecord): ?>



                <div class="box-body">
                    <div id='gallery'>
                        <a href="<?= Yii::getAlias('@web') . '/images/work/' . $model->id . '/' . $model->avatar->path ?> "><img
                                style="width: 240px" class="news-img"
                                src="<?= Yii::getAlias('@web') . '/images/work/' . $model->id . '/cropped/' . $model->avatar->path ?>"></a>
                    </div>
                </div>
            <?php endif; ?>

        </div>

        <div class="box-footer">
            <div class="form-btn">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
