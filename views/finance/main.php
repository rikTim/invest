<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Investor;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <div class="box">
        <?php if(Yii::$app->user->can('admin')): $znak = '₴'?>
        <?php else: $znak= '$'?>
        <?php endif;?>
        <div class="box-header with-border">
            <div class="col-md-12">
                <div
                    class="pull-left"><?= Html::a('Добавить Инвестиции', ['create-money'], ['class' => 'btn btn-success']) ?>
                </div>
                <div class="pull-right">
                    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                </div>
            </div>
            <div class="clearboth"></div>
            <div class="col-md-12" style="height:20px;"></div>
            <div class="col-md-3 "><a href="<?= \yii\helpers\Url::toRoute(['finance/money']) ?>"><h3>
                        Инвестировано:<?= $sum .$znak?></h3></a></div>
            <div class="col-md-1"></div>
            <?php if (!Yii::$app->user->can('admin')):?>
            <div class="col-md-3 "><a
                    href="<?= \yii\helpers\Url::toRoute(['investor-project/index', 'id' => Yii::$app->user->id]) ?>">
                    <h3> Потрачено:<?= $invest . $znak?></h3></a></div>
            <?php else:?>
                <div class="col-md-3 "><a
                        href="<?= \yii\helpers\Url::toRoute(['investor-project/index']) ?>">
                        <h3>Потрачено:<?= $invest .$znak?></h3></a></div>
            <?php endif;?>
            <div class="col-md-1"></div>
            <div class="col-md-3 "><h3>Остаток:<?= $sum - $invest . $znak ?>></h3></div>
            <div class="col-md-1"></div>
        </div>

        <div class="box-body">

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [

                    'name',
//                    'kurs',
                    [
                        'attribute' => 'datetime',
                        'filter' => \yii\jui\DatePicker::widget(['model' => $searchModel, 'attribute' => 'datetime',
                            'language' => 'ru',
                            'dateFormat' => 'yyyy-MM-dd',
                            'options' => ['class' => 'form-control'],
                        ]),
                        'content' => function ($data) {
                            if (!empty($data->datetime)) {
                                return date('d.m.Y H:i:s', $data->datetime);
                            }
                        }
                    ],

                    [
                        'attribute' => 'avatar_id',
                        'format' => 'html',
                        'label' => 'Фото',
                        'value' => function ($data) {
                            return '<img src=' . Yii::getAlias('@web') . '/images/project/' . $data->id . '/cropped/' . $data->avatar->path . '>';
                        }
                    ],


//                    [
//                        'attribute' => 'finance',
//                        'label' => 'Инвестиции',
//                        'format' => 'html',
//                        'value' => function ($data) {
//                            if (Yii::$app->user->can('admin')) {
//                                return InvegetInvestion($data->id);
//                            } else {
//                                return $data->getInvestionusd($data->id) . '$ (' . $data->getInvestion($data->id) . '₴)';
//                            }
//                        }
//                    ],


                    [
                        'attribute' => 'finance',
                        'label' => 'Потраченые на материалы',
                        'value' => function ($data) {
                            if (Yii::$app->user->can('admin')) {
                                return $data->getMoneyMaterialMinus($data->id);
                            } else {
                                return '(' . $data->getMoneyMaterialMinus($data->id) . '₴)';
                            }

                        }
                    ],

                    [
                        'attribute' => 'finance',
                        'label' => 'Не оплаченые материалы',
                        'value' => function ($data) {
                            if (Yii::$app->user->can('admin')) {
                                return $data->getMoneyMaterialNoPaid($data->id);
                            } else {
                                return '(' . $data->getMoneyMaterialNoPaid($data->id) . '₴)';
                            }

                        }
                    ],


                    [
                        'attribute' => 'finance',
                        'label' => 'Потраченые на работы',
                        'format' => 'html',
                        'value' => function ($data) {
                            if (Yii::$app->user->can('admin')) {
                                return $data->getMoneyWorkMinus($data->id);
                            } else {
                                return '(' . $data->getMoneyWorkMinus($data->id) . '₴)';
                            }

                        }
                    ],


                    [
                        'attribute' => 'finance',
                        'label' => 'Не оплаченые работы',
                        'value' => function ($data) {
                            if (Yii::$app->user->can('admin')) {
                                return $data->getMoneyWorkNoPaid($data->id);
                            } else {
                                return '(' . $data->getMoneyWorkNoPaid($data->id) . '₴)';
                            }

                        }
                    ],

//                    [
//                        'attribute' => 'finance',
//                        'label' => 'Остаток в кассе',
//                        'value' => function ($data) {
//
//                            if (Yii::$app->user->can('admin')) {
//                                return $data->getSumAll($data->id);
//                            } else {
//                                return '(' . $data->getSumAll($data->id) . '₴)';
//                            }
//                        }
//                    ],
//                    [
//                        'attribute' => 'finance',
//                        'format' => 'html',
//                        'label' => 'Сумма',
//                        'value' => function ($data) {
//                            return $data->getSum($data->id);
//                        }
//                    ],


                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '70'],
                        'template' => '{view} {link}{material}',

                        'buttons' => [
                            'link' => function ($url, $model) {
                                return Html::a('&nbsp;&nbsp;<span class="fa fa-building"></span>', $url, [
                                    'title' => Yii::t('app', 'Work'),
                                ]);
                            },
                            'money' => function ($url, $model) {
                                return Html::a('&nbsp;&nbsp;<span class="glyphicon glyphicon-usd"></span>&nbsp;', $url, [
                                    'title' => Yii::t('app', 'Work'),
                                ]);
                            },
                            'material' => function ($url, $model) {
                                return Html::a('&nbsp;&nbsp;<span class="glyphicon glyphicon-folder-open"></span>', $url, [
                                    'title' => Yii::t('app', 'Work'),
                                ]);
                            },


                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            switch ($action) {
                                case 'link':
                                    $url = \yii\helpers\Url::toRoute(['finance/index', 'id' => $model->id]);
                                    return $url;

                                case 'money':
                                    $url = \yii\helpers\Url::toRoute(['finance/money', 'id' => $model->id]);
                                    return $url;
                                case 'view':
                                    $url = \yii\helpers\Url::toRoute(['project/view', 'id' => $model->id]);
                                    return $url;
                                case 'material':
                                    $url = \yii\helpers\Url::toRoute(['finance/material', 'id' => $model->id]);
                                    return $url;
                            }
                        }

                    ],

                ],
            ]); ?>
        </div>

    </div> <!--end box -->

</div>
