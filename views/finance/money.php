<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InvestorMoneySeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Investor Moneys';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="investor-money-index">

    <div class="box">

        <div class="box-header with-border">
            <div class="pull-left">
<!--                --><?//= Html::a('Создать', ['create-money'], ['class' => 'btn btn-success']) ?>
            </div>
            <div class="pull-right">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
        </div>

        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                'columns' => [
//                    ['class' => 'yii\grid\SerialColumn'],

//                    'id',
                    'money_usd',
                    [
                        'attribute' => 'datetime',
                        'filter' => \yii\jui\DatePicker::widget(['model' => $searchModel, 'attribute' => 'datetime',
                            'language' => 'ru',
                            'dateFormat' => 'yyyy-MM-dd',
                            'options' => ['class' => 'form-control'],
                        ]),
                        'content' => function ($data) {
                            if (!empty($data->datetime)) {
                                return date('d.m.Y H:i:s', $data->datetime);
                            }
                        }
                    ],
                    'kurs',
//                    [
//                        'attribute'=>'project_id',
//                        'value'=>function($data) {
//                            return $data->project->name;
//                        }
//                    ],

                     'investor.name',
                ],
            ]); ?>
        </div>

    </div> <!--end box -->

</div>
