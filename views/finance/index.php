<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\WorkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Works';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-index">

    <div class="box">

        <div class="box-header with-border">
            <div class="pull-left">
<!--                --><?//= Html::a('Создать', ['create','project_id'=> $id ], ['class' => 'btn btn-success']) ?>
            </div>
            <div class="pull-right">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                    </div>
        </div>

        <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [


//
//            [
//                'attribute'=>'operator_id',
//                'value'=>function($data){
//                    return $data->operator->name;
//                }
//            ],

            [
                'attribute'=>'stage_id',
                'value'=>function($data){
                    return $data->stage->name;
                }
            ],
            [
                'attribute'=>'work_id',
                'value'=>function($data){
                    return $data->work->name;
                }
            ],


            'money',

            [
                'attribute'=>'success',
                'label' => 'Оплачено',


            ],
//            [
//                'attribute' => 'status_id',
//                'label'=>'Статус',
//                'format' => 'raw',
//                'value' => function($data){
//
//                    $status = $data->status->name;
//                    switch ($status) {
//                        case 'новый':
//                            return '<span class="label bg-green "> ' . $data->status->name . '</span>';
//                            break;
//                        case 'подтвержденный':
//                            return '<span class="label bg-blue "> ' . $data->status->name . '</span>';
//                            break;
//                        case 'законченый':
//                            return '<span class="label bg-yellow "> ' . $data->status->name . '</span>';
//                            break;
//                        default:
//                            return $data->status->name;
//                            break;
//                    }
//                }
//            ],


            [
                'attribute' => 'datetime',
                'filter' => \yii\jui\DatePicker::widget(['model' => $searchModel, 'attribute' => 'datetime',
                    'language' => 'ru',
                    'dateFormat' => 'yyyy-MM-dd',
                    'options' => ['class' => 'form-control'],
                ]),
                'content' => function ($data) {
                    if (!empty($data->datetime)) {
                        return date('d.m.Y H:i:s', $data->datetime);
                    }
                }
            ],


            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '70'],
                'template' => '{view}',


//                'buttons' => [
//                    'money' => function ($url, $model) {
//                        return Html::a('<span class="btn btn-primary">Детальная статистика</span>', $url, [
//                            'title' => Yii::t('app', 'Work'),
//                        ]);
//                    },
//
//
//                ],
//                'urlCreator' => function ($action, $model, $key, $index) {
//                    switch($action){
//                        case 'money':
//                            $url = \yii\helpers\Url::toRoute(['finance/money','id'=>$model->id]);
//                            return $url;
//                    }
//                }


            ],
        ],
    ]); ?>
        </div>

    </div> <!--end box -->

</div>
