<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InvestorMoneySeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Investor Moneys';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="investor-money-index">

    <div class="box">

        <div class="box-header with-border">
            <div class="pull-left">
                <?= Html::a('Create Investor Money', ['create'], ['class' => 'btn btn-success']) ?>
            </div>
            <div class="pull-right">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                    </div>
        </div>

        <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'money',
            'datetime',
            'kurs',
            'project_id',
            // 'investor_id',

            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '70'],
                'template' => '{view} {update} {delete} {link}',
            ],
        ],
    ]); ?>
        </div>

    </div> <!--end box -->

</div>
