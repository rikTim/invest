<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\InvestorMoney */

$this->title = 'Update Investor Money' . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Investor Moneys', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="investor-money-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
