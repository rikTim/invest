<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\InvestorMoney */

$this->title = 'Create Investor Money';
$this->params['breadcrumbs'][] = ['label' => 'Investor Moneys', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="investor-money-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
