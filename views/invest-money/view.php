<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\InvestorMoney */

$this->title = 'View Investor Money' . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Investor Moneys', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="investor-money-view">

    <div class="box">

        <div class="box-header with-border">
        <?= Html::a('Manage', ['index'], ['class' => 'btn btn-warning']) ?>
        <?= Html::a('Create', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>

        </div>

        <div class="box-body">

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
            'money',
            'datetime',
            'kurs',
            'project_id',
            'investor_id',
            ],
        ]) ?>

        </div>

    </div>

</div>
