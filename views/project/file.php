<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <div class="box box-default">

        <div class="box-body">
            <?php if (Yii::$app->user->can('admin') || Yii::$app->user->can('moder')): ?>
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'name')->hiddenInput(['value'=>$model->name])->label(false) ?>

            <?= $form->field($upload, 'files[]')->fileInput(['multiple' => true]) ?>




            <div class="box-footer">
                <div class="form-btn">
                    <?= Html::submitButton( 'Добавить Файли', ['class' => 'btn btn-success' ]) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

            <?php endif;?>

            <?php if (!empty($files)): ?>
                <div class="box-body">
                    <h3>Документы</h3>
                    <?php foreach ($files as $count=> $file): ?>
                        <div>
                            <img  style="width: 40px" src="<?=Yii::getAlias('@web'). '/images/file.png '?>">
                            <a class="btn btn-default" href="<?= Yii::getAlias('@web') . '/images/file/' . $file->project_id . '/' . $file->path ?>" download="<?=  $file->path ?>"> Скачать файл_<?= $count?></a>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>

    </div>
