<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <div class="box box-default">

        <div class="box-body">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<!--            --><?//= $form->field($model, 'kurs')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'status')->dropDownList($status) ?>


            <?= $form->field($upload, 'imageFile')->fileInput()->label(false) ?>



        </div>

        <?php if (!$model->isNewRecord): ?>
            <div class="box-body">
                <div id='gallery'>
                    <a href="<?= Yii::getAlias('@web') . '/images/project/' . $model->id . '/' . $model->avatar->path ?> "><img
                            style="width: 240px" class="news-img"
                            src="<?= Yii::getAlias('@web') . '/images/project/' . $model->id . '/cropped/' . $model->avatar->path ?>"></a>
                </div>
            </div>
        <?php endif; ?>

        <div class="box-footer">
            <div class="form-btn">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
