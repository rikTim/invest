<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Project */

$this->title = 'View Project' . ' #' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-view">

    <div class="box">

        <div class="box-header with-border">


            <?php if(Yii::$app->user->can('admin')):?>
        <?= Html::a('Главная', ['index'], ['class' => 'btn btn-warning']) ?>

        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?php else:?>
            <?= Html::a('Главная', ['finance/main'], ['class' => 'btn btn-warning']) ?>
            <?php endif;?>
<!--        --><?//= Html::a('Delete', ['delete', 'id' => $model->id], [
//            'class' => 'btn btn-danger',
//            'data' => [
//                'confirm' => 'Are you sure you want to delete this item?',
//                'method' => 'post',
//            ],
//        ]) ?>

        </div>

        <div class="box-body">
        <?php $model->datetime = date('d.m.Y H:i:s', $model->datetime)?>
            <?php $status = \app\models\Status::find()->where(['id'=>$model->status])->one();
            $model->status = $status->name?>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
//                'id',
            'name',
//            'kurs',
//                [
//                    'attribute' => 'datetime',
//
//                    'content' => function ($data) {
//                        if (!empty($data->datetime)) {
//                            return date('d.m.Y H:i:s', $data->datetime);
//                        }
//                    }
//                ],
            'datetime',
            'status',
            ],
        ]) ?>


            <?php if (!empty($photos)): ?>
                <div class="box-body">
                    <div id='gallery'>
                        <?php foreach ($photos as $photo): ?>

                            <a href="<?= Yii::getAlias('@web') . '/images/stage/' . $photo->stage_id . '/' . $photo->path ?> "><img
                                    style="width: 240px" class="news-img"
                                    src="<?= Yii::getAlias('@web') . '/images/stage/' . $photo->stage_id . '/cropped/' . $photo->path ?>"></a>

                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if (!empty($files)): ?>
                <div class="box-body">
                    <h3>Документы</h3>
                    <?php foreach ($files as $count=> $file): ?>
                        <div>
                            <img  style="width: 40px" src="<?=Yii::getAlias('@web'). '/images/file.png '?>">
                            <a class="btn btn-default" href="<?= Yii::getAlias('@web') . '/images/file/' . $file->project_id . '/' . $file->path ?>" download="<?=  $file->path ?>"> Скачать файл_<?= $count?></a>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>

    </div>

</div>
