<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <div class="box">

        <div class="box-header with-border">
            <div class="pull-left">
                <?php if (Yii::$app->user->can('admin')): ?>
                    <?= Html::a('Создать обьект', ['create'], ['class' => 'btn btn-success']) ?>


                <?php endif; ?>
            </div>
            <div class="pull-right">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
        </div>

        <div class="box-body">

            <?= GridView::widget(['dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => ['name',



                    [
                        'attribute' => 'datetime',
                        'filter' => \yii\jui\DatePicker::widget(['model' => $searchModel, 'attribute' => 'datetime',
                            'language' => 'ru',
                            'dateFormat' => 'yyyy-MM-dd',
                            'options' => ['class' => 'form-control'],
                        ]),
                        'content' => function ($data) {
                            if (!empty($data->datetime)) {
                                return date('d.m.Y H:i:s', $data->datetime);
                            }
                        }
                    ],

                    [
                        'attribute' => 'status',
                        'label'=>'Статус',
                        'format' => 'raw',
                        'value' => function($data){

                            $status = $data->statuses->name;
                            switch ($status) {
                                case 'новый':
                                    return '<span class="label bg-yellow "> ' . $data->statuses->name . '</span>';
                                    break;
                                case 'подтвержденный':
                                    return '<span class="label bg-green "> ' . $data->statuses->name . '</span>';
                                    break;
                                case 'законченый':
                                    return '<span class="label bg-blue "> ' . $data->statuses->name . '</span>';
                                    break;
                                default:
                                    return $data->status->name;
                                    break;
                            }
                        }
                    ],
//                    [
//                        'attribute' =>'kurs',
//                        'label' => 'Процент',
//                        'format' => 'raw',
//                        'value' => function($data){
//                            return $data->getPersent($data->id);
//                        }
//                    ],



                    [
                        'attribute' => 'avatar_id',
                        'format' => 'html',
                        'label' => 'Фото',
                        'value' => function ($data) {
                            return '<img src=' . Yii::getAlias('@web') . '/images/project/' . $data->id . '/cropped/' . $data->avatar->path . '>';
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '60'],
                        'template' => '{view} {update}  {image}{file}',


                        'buttons' => [
                            'link' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-usd"></span>', $url, [
                                    'title' => Yii::t('app', 'Work'),
                                ]);
                            },
                            'image' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-picture"></span>', $url, [
                                    'title' => Yii::t('app', 'Work'),
                                ]);
                            },

                            'file' => function ($url, $model) {
                                return Html::a('&nbsp; <span class="glyphicon glyphicon-folder-open"></span>', $url, [
                                    'title' => Yii::t('app', 'File'),
                                ]);
                            },

                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            switch($action){
                                case 'link':
                                    $url = \yii\helpers\Url::toRoute(['investor/index','id'=>$model->id]);
                                    return $url;
                                case 'update':
                                    $url = \yii\helpers\Url::toRoute(['project/update','id'=>$model->id]);
                                    return $url;
                                case 'view':
                                    $url = \yii\helpers\Url::toRoute(['project/view','id'=>$model->id]);
                                    return $url;
                                case 'delete':
                                    $url = \yii\helpers\Url::toRoute(['project/delete','id'=>$model->id]);
                                    return $url;
                                case 'image':
                                    $url = \yii\helpers\Url::toRoute(['project/image','id'=>$model->id]);
                                    return $url;
                                case 'file':
                                    $url = \yii\helpers\Url::toRoute(['project/file','id'=>$model->id]);
                                    return $url;
                            }


                        }
                    ],
                ],
            ]); ?>
        </div>

    </div> <!--end box -->

</div>
