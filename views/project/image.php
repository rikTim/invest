<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <div class="box box-default">

        <div class="box-body">
            <?php if (Yii::$app->user->can('admin') || Yii::$app->user->can('moder')): ?>
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($photoModel, 'stage_id')->dropDownList($stage) ?>


            <?= $form->field($upload, 'files[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>




            <div class="box-footer">
                <div class="form-btn">
                    <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
            <?php endif;?>
            <?php if (!empty($photos)): ?>
                <div class="box-body">
                    <div id='gallery'>
                        <?php foreach ($photos as $photo): ?>

                            <a href="<?= Yii::getAlias('@web') . '/images/stage/' . $photo->stage_id . '/' . $photo->path ?> "><img
                                    style="width: 240px" class="news-img"
                                    src="<?= Yii::getAlias('@web') . '/images/stage/' . $photo->stage_id . '/cropped/' . $photo->path ?>"></a>

                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>

    </div>
