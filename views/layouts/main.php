<?php

use yii\helpers\Html;
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;
use app\themes\adminLTE\components\ThemeNav;

?>
<?php $this->beginContent('@app/themes/adminLTE/layouts/main.php'); ?>

    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?php echo Yii::$app->request->baseUrl; ?>/images/user_accounts.png" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>
                        <?php
                        $info[] = Yii::t('app','Здравствуйте');

                        if(!Yii::$app->user->isGuest) {
                            $user = \app\models\Users::findOne(Yii::$app->user->id);
                            $info[] = ucfirst($user->name);
                        }

                        echo implode(', ', $info);

                        ?>
                    </p>
                    <a><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>

            <!-- sidebar menu: : style can be found in sidebar.less -->
            <?php
            echo Menu::widget([
                'encodeLabels'=>false,
                'options' => [
                    'class' => 'sidebar-menu'
                ],
                'items' => [
                    ['label'=>Yii::t('app','Панель навигации'), 'options'=>['class'=>'header']],

                    ['label' => ThemeNav::link('Проекты', 'fa fa-home'), 'url' => ['project/index'], 'visible'=>!Yii::$app->user->isGuest && Yii::$app->user->can('admin')],

                    ['label' => ThemeNav::link('Работники', 'fa  fa-user'), 'url' => ['workers/index'], 'visible'=>!Yii::$app->user->isGuest && Yii::$app->user->can('admin')],
                    ['label' => ThemeNav::link('Инвестора', 'fa fa-user'), 'url' => ['investor-project/main'], 'visible'=>!Yii::$app->user->isGuest && Yii::$app->user->can('admin')],
                    ['label' => ThemeNav::link('Бригады', 'fa  fa-users'), 'url' => ['brigade/index'], 'visible'=>!Yii::$app->user->isGuest && Yii::$app->user->can('admin')],
                    ['label' => ThemeNav::link('Материалы', 'fa fa-folder-open'), 'url' => ['material/index'], 'visible'=>!Yii::$app->user->isGuest && Yii::$app->user->can('admin') || Yii::$app->user->can('operatorMaterial')],


                    ['label' => ThemeNav::link('Финансовая статистика', 'fa fa-money'), 'url' => ['finance/main'], 'visible'=>!Yii::$app->user->isGuest && Yii::$app->user->can('investor') || Yii::$app->user->can('admin') ],
//                    ['label' => ThemeNav::link('Финансы', 'fa fa-money'), 'url' => ['investor-project/index','id'=>Yii::$app->user->id], 'visible'=>!Yii::$app->user->isGuest && Yii::$app->user->can('investor') ],

                    ['label' => ThemeNav::link('Материалы и работы', 'fa fa-briefcase'), 'url' => ['work/main'], 'visible'=>!Yii::$app->user->isGuest && Yii::$app->user->can('moder') || Yii::$app->user->can('admin') || Yii::$app->user->can('operatorMaterial') || Yii::$app->user->can('operatorWork')],
                    ['label' => ThemeNav::link('Пользователи', 'fa  fa-user'), 'url' => ['users/index'], 'visible'=>!Yii::$app->user->isGuest && Yii::$app->user->can('admin')],

                    ['label' => ThemeNav::link('Этапы работ', 'fa fa-industry'), 'url' => ['stage/index'], 'visible'=>!Yii::$app->user->isGuest && Yii::$app->user->can('admin')],
                    ['label' => ThemeNav::link('Список работ', 'fa fa-industry'), 'url' => ['works/index'], 'visible'=>!Yii::$app->user->isGuest && Yii::$app->user->can('admin')],

                ],
            ]);
            ?>

        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                'homeLink' => false,
            ]) ?>
        </section>


        <!-- Main content -->
        <section class="content" style="margin-top: 20px">
            <?php echo $content; ?>
        </section><!-- /.content -->

    </div><!-- /.right-side -->
<?php $this->endContent();?>

<?php \yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'galleryShow'],
    'header' => '<h4>Просмотр Фото</h4>',
    'id' => 'galleryShow',
    'size' => 'modal-lg',

]); ?>
    <div id='showGallery'></div>
<?php \yii\bootstrap\Modal::end(); ?>