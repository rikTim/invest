<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Brigade */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="brigade-form">

	<div class="box box-default">

		<div class="box-body">

	    <?php $form = ActiveForm::begin(); ?>

	    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<!--    --><?//= $form->field($model, 'create_datetime')->textInput(['maxlength' => true]) ?>

<!--    --><?//= $form->field($model, 'finish_datetime')->textInput(['maxlength' => true]) ?>

<!--    --><?//= $form->field($model, 'status_id')->textInput() ?>

		</div>

	    <div class="box-footer">
			<div class="form-btn">
	        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    	</div>
	    </div>

    <?php ActiveForm::end(); ?>

    </div>

</div>
