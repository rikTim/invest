<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BrigadeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Brigades';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brigade-index">

    <div class="box">

        <div class="box-header with-border">
            <div class="pull-left">
                <?= Html::a('Добавить бригаду', ['create'], ['class' => 'btn btn-success']) ?>
            </div>
            <div class="pull-right">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                    </div>
        </div>

        <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'name',
            [     'attribute' => 'create_datetime',
                'filter' => \yii\jui\DatePicker::widget(['model' => $searchModel, 'attribute' => 'create_datetime',
                    'language' => 'ru',
                    'dateFormat' => 'yyyy-MM-dd',
                    'options' => ['class' => 'form-control'],
                ]),
                'content' => function ($data) {
                    if (!empty($data->create_datetime)) {
                        return date('d.m.Y H:i:s', $data->create_datetime);
                    }
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '70'],
                'template' => '{view} {update}{delete}{users}',


                'buttons' => [
                    'users' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-user"></span>', $url, [
                            'title' => Yii::t('app', 'Work'),
                        ]);
                    },


                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    switch($action){
                        case 'users':
                            $url = \yii\helpers\Url::toRoute(['team/index','id'=>$model->id]);
                            return $url;
                        case 'update':
                            $url = \yii\helpers\Url::toRoute(['brigade/update','id'=>$model->id]);
                            return $url;
                        case 'view':
                            $url = \yii\helpers\Url::toRoute(['brigade/view','id'=>$model->id]);
                            return $url;
                        case 'delete':
                            $url = \yii\helpers\Url::toRoute(['brigade/delete','id'=>$model->id]);
                            return $url;

                    }


                }
            ],
        ],
    ]); ?>
        </div>

    </div> <!--end box -->

</div>
