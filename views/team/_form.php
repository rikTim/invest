<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Team */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="team-form">

	<div class="box box-default">

		<div class="box-body">

	    <?php $form = ActiveForm::begin(); ?>
		<?php if(!empty($user)):?>
	    <?= $form->field($model, 'user_id')->dropDownList($user) ?>
		<?php else:?>
			<h4><?= Html::a('Добавить Работника', ['create'], ['class' => 'btn btn-success']) ?> - Все созданные находятся в бригадах</h4>
		<?php endif;?>
    <?= $form->field($model, 'brigade_id')->hiddenInput(['value'=>$id])->label(false) ?>

		</div>

	    <div class="box-footer">
			<div class="form-btn">
				<?php if(!empty($user)):?>
	        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
				<?php endif;?>
	    	</div>
	    </div>

    <?php ActiveForm::end(); ?>

    </div>

</div>
