<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TeamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Teams';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-index">

    <div class="box">

        <div class="box-header with-border">
            <div class="pull-left">
                <?= Html::a('Добавить работника в бригаду', ['create','id'=>$id], ['class' => 'btn btn-success']) ?>
            </div>
            <div class="pull-right">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                    </div>
        </div>

        <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'user_id',
            'brigade_id',

            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '70'],
                'template' => '{view} {update} {delete} {link}',
            ],
        ],
    ]); ?>
        </div>

    </div> <!--end box -->

</div>
