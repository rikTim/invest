<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\WorkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Works';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-index">

    <div class="box">

        <div class="box-header with-border">
            <div class="pull-left">
                <?= Html::a('Создать', ['create', 'project_id' => $model->id], ['class' => 'btn btn-success']) ?>
            </div>
            <div class="pull-right">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
        </div>

        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [


                    [
                        'attribute' => 'brigade_id',
                        'value' => function ($data) {
                            return $data->brigade->name;
                        }
                    ],

                    [
                        'attribute' => 'stage_id',
                        'value' => function ($data) {
                            return $data->stage->name;
                        }
                    ],
                    [
                        'attribute' => 'work_id',
                        'value' => function ($data) {
                            return $data->work->name;
                        }
                    ],
                    [
                        'attribute' => 'status_id',
                        'label' => 'Статус',
                        'format' => 'raw',
                        'value' => function ($data) {

                            $status = $data->status->name;
                            switch ($status) {
                                case 'новый':
                                    return '<span class="label bg-green "> ' . $data->status->name . '</span>';
                                    break;
                                case 'подтвержденный':
                                    return '<span class="label bg-blue "> ' . $data->status->name . '</span>';
                                    break;
                                case 'законченый':
                                    return '<span class="label bg-yellow "> ' . $data->status->name . '</span>';
                                    break;
                                default:
                                    return $data->status->name;
                                    break;
                            }
                        }
                    ],
                    'money',

                    [
                        'attribute' => 'success',
                        'label' => 'Оплачено',


                    ],

                    'price',
                    'number',

                    [
                        'attribute' => 'datetime',
                        'filter' => \yii\jui\DatePicker::widget(['model' => $searchModel, 'attribute' => 'datetime',
                            'language' => 'ru',
                            'dateFormat' => 'yyyy-MM-dd',
                            'options' => ['class' => 'form-control'],
                        ]),
                        'content' => function ($data) {
                            if (!empty($data->datetime)) {
                                return date('d.m.Y H:i:s', $data->datetime);
                            }
                        }
                    ],


                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '70'],
                        'template' => '{success} {view} {update}{money}{question}',
                        'visibleButtons' => [
                            'update' => function ($model, $key, $index) {
                                return $model->status_id == 1;
                            },
                            'money' => function ($model, $key, $index) {
                                return $model->success < $model->money;
                            },
                            'success' => function ($model, $key, $index) {
                                return $model->status_id == 1 && Yii::$app->user->can('admin');
                            },
                        ],

                        'buttons' => [
                            'money' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-usd"></span>', $url, [
                                    'title' => Yii::t('app', 'Work'),
                                ]);
                            },
                            'question' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-question-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Question'),
                                ]);
                            },
                            'success' => function ($url, $model) {
                                return \yii\helpers\Html::a('<span class="glyphicon glyphicon-ok"></span>&nbsp;', $url, [
                                    'role' => 'modal-remote', 'title' => 'Success', 'data-toggle' => 'tooltip'
                                ]);
                            },


                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            switch ($action) {
                                case 'money':
                                    $url = \yii\helpers\Url::toRoute(['work-money/index', 'id' => $model->id]);
                                    return $url;
                                case 'update':
                                    $url = \yii\helpers\Url::toRoute(['work/update', 'id' => $model->id]);
                                    return $url;
                                case 'view':
                                    $url = \yii\helpers\Url::toRoute(['work/view', 'id' => $model->id]);
                                    return $url;
                                case 'question':
                                    $url = \yii\helpers\Url::toRoute(['work-comment/index', 'id' => $model->id]);
                                    return $url;
                                case 'success':
                                    $url = \yii\helpers\Url::to(['work/success', 'id' => $model->id,]);
                                    return $url;

                            }


                        }

                    ],
                ],
            ]); ?>
        </div>

    </div> <!--end box -->

</div>
<?php \yii\bootstrap\Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php \yii\bootstrap\Modal::end(); ?>
