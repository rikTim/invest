<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FabricTest */
?>
<div class="work-index">

    <div class="box">
        <div class="box-body">
            <div class="fabric-test-view">

                <?= DetailView::widget([
                    'model' => $workModel,
                    'attributes' => [
//                'id',
                        'datetime',
                        'brigade_id',
                        'status_id',
                        'stage_id',
                        'work_id',
                        'note:ntext',
                        'money',
                        'price',
                        'number',
                        'project_id',
                    ],
                ]) ?>



                <?php $form = \yii\widgets\ActiveForm::begin(); ?>
                <?php if($money == $workModel->money  ):?>
                    <?= $form->field($model, 'kurs')->hiddenInput(['value'=>1])->label(false); ?>
                <?php else:?>
                <?= $form->field($model, 'money')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'investor_id')->dropDownList($user,['prompt' => 'Виберите инвестора']) ?>
<!--                --><?//= $form->field($model, 'kurs')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>
                <?php endif;?>

                    <div class="form-group">
                        <?= \yii\helpers\Html::submitButton($model->isNewRecord ? ' Подтвердить' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>


                <?php \yii\widgets\ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>
