<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <div class="box">

        <div class="box-header with-border">
            <div class="pull-left">

            </div>
            <div class="pull-right">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
        </div>

        <div class="box-body">

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
                'columns' => [

                    'name',
//                    'kurs',
                    [
                        'attribute' => 'datetime',
                        'filter' => \yii\jui\DatePicker::widget(['model' => $searchModel, 'attribute' => 'datetime',
                            'language' => 'ru',
                            'dateFormat' => 'yyyy-MM-dd',
                            'options' => ['class' => 'form-control'],
                        ]),
                        'content' => function ($data) {
                            if (!empty($data->datetime)) {
                                return date('d.m.Y H:i:s', $data->datetime);
                            }
                        }
                    ],
                    [
                        'attribute' => 'avatar_id',
                        'format' => 'html',
                        'label' => 'Фото',
                        'value' => function ($data) {
                            return '<img src=' . Yii::getAlias('@web') . '/images/project/' . $data->id . '/cropped/' . $data->avatar->path . '>';
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '70'],
                        'template' => ' {link}{material}',
                        'visibleButtons'=> [
                            'link'=>function ($model, $key, $index) {
                                if(Yii::$app->user->can('admin') || Yii::$app->user->can('operatorWork')){

                                    return  true;
                                }else return false;
                            },
                            'material'=> function ($model, $key, $index) {
                                if(Yii::$app->user->can('admin') || Yii::$app->user->can('operatorMaterial')){
//                                    print_r(1);die();
                                    return  true;
                                }

                                else return false;

                            },
                        ],

                        'buttons' => [
                            'link' => function ($url, $model) {
                                return Html::a('<i class="fa fa-building" aria-hidden="true"></i>&nbsp&nbsp', $url, [
                                    'title' => Yii::t('app', 'Work'),
                                ]);
                            },
                            'material' => function ($url, $model) {
                                return Html::a('<i class="fa fa-folder-open" aria-hidden="true"></i>', $url, [
                                    'title' => Yii::t('app', 'Material'),
                                ]);
                            },


                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            switch($action){
                                case 'link':
                                    $url = \yii\helpers\Url::toRoute(['work/index','id'=>$model->id]);
                                    return $url;
                                case 'material':
                                    $url = \yii\helpers\Url::toRoute(['fabric-test/index','id'=>$model->id]);
                                    return $url;
                            }
                        }

                    ],

                ],
            ]); ?>
        </div>

    </div> <!--end box -->

</div>
