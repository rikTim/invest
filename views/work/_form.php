<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Work */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="work-form">

    <div class="box box-default">

        <div class="box-body">

            <?php $form = ActiveForm::begin([
                'options' => [
                    'enctype' => 'multipart/form-data',
                ]
            ]); ?>

            <?= $form->field($model, 'brigade_id')->dropDownList($brigade) ?>
            <?php if(Yii::$app->user->can('admin')):?>
            <?= $form->field($model, 'status_id')->dropDownList($status) ?>
            <?php else:?>
                <?= $form->field($model, 'status_id')->hiddenInput(['value'=>1])->label(false); ?>
            <?php endif;?>
            <?= $form->field($model, 'stage_id')->dropDownList($stage, ['id'=>'cat-id','prompt' => 'Выберите ступень']) ?>

            <?php if(Yii::$app->user->can('admin')):?>
            <h4>Чтобы добавить не существующие в списке "Выполненые работы" воспользуйтесь этой кнопкой &nbsp;<?= Html::a('Добавить', ['stage' ], ['class' => 'btn btn-success','target'=>'blank']) ?></h4>
            <?php endif;?>
            <?= $form->field($model, 'work_id')->widget(\kartik\depdrop\DepDrop::classname(), [
                'options' => ['id' => 'subcat-id'],
                'pluginOptions' => [
                    'depends' => ['cat-id'],
                    'placeholder' => 'Выберите...',
                    'url' => \yii\helpers\Url::to(['/work/work'])
                ]
            ]); ?>

            <?= $form->field($model, 'note')->textarea(['rows' => 6]) ?>


            <?= $form->field($model, 'price')->textInput(['maxlength' => true,'id'=>'price']) ?>


            <?= $form->field($model, 'number')->textInput(['maxlength' => true,'id'=>'number']) ?>


            <?= $form->field($model, 'money')->textInput(['maxlength' => true,'id'=>'sum-money','disabled' => 'disabled']) ?>




            <?= $form->field($upload, 'imageFile')->fileInput()->label('Фото Стоимости') ?>

            <?= $form->field($model, 'project_id')->hiddenInput(['value' => $project_id])->label(false); ?>

            <?= $form->field($file, 'files[]')->fileInput(['multiple' => true])->label('Документы') ?>
<!--            <br>-->
<!--            --><?php //if($model->isNewRecord):?>
<!--            <h3>Деньги</h3>-->
<!---->
<!---->
<!---->
<!--            --><?//= $form->field($money, 'money')->textInput(['maxlength' => true]) ?>
<!---->
<!--            --><?//= $form->field($money, 'text')->textarea(['rows' => 6]) ?>
<!---->
<!---->
<!--            --><?php //endif; ?>

            <?php if (!$model->isNewRecord && !empty($model->money_picture)): ?>
                <div class="box-body">
                    <div id='gallery'>
                        <a href="<?= Yii::getAlias('@web') . '/images/work/' . $model->id . '/' . $model->avatar->path ?> "><img
                                style="width: 240px" class="news-img"
                                src="<?= Yii::getAlias('@web') . '/images/work/' . $model->id . '/cropped/' . $model->avatar->path ?>"></a>
                    </div>
                </div>
            <?php endif; ?>

        </div>

        <div class="box-footer">
            <div class="form-btn">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
