<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Work */

$this->title = 'Update Work' . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Works', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="work-update">

    <?= $this->render('_form', [
        'model' => $model,
        'project_id' => $model->project_id,
        'status' => $status,
        'stage'=> $stage,
        'upload' => $upload,
        'file' => $file,
        'brigade'=> $brigade,
    ]) ?>

</div>
