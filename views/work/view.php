<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Work */

$this->title = 'View Work' . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Works', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-view">

    <div class="box">

        <div class="box-header with-border">
            <?= Html::a('Назад', ['index', 'id' => $project_id], ['class' => 'btn btn-warning']) ?>
            <?= Html::a('Создать', ['create', 'project_id' => $project_id], ['class' => 'btn btn-success']) ?>
            <?php if (!$model->status_id == 2): ?>
                <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?php endif ?>
            <!--        --><? //= Html::a('Delete', ['delete', 'id' => $model->id], [
            //            'class' => 'btn btn-danger',
            //            'data' => [
            //                'confirm' => 'Are you sure you want to delete this item?',
            //                'method' => 'post',
            //            ],
            //        ]) ?>

        </div>

        <div class="box-body">

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
//                'id',
                    'datetime',
                    'brigade_id',
                    'status_id',
                    'stage_id',
                    'work_id',
                    'note:ntext',
                    'money',
                    'price',
                    'number',
                    'project_id',
                ],
            ]) ?>

            <?php if (!empty($model->money_picture)): ?>
                <div class="box-body">
                    <div id='gallery'>


                        <a href="<?= Yii::getAlias('@web') . '/images/work/' . $model->id . '/' . $model->avatar->path ?> "><img
                                style="width: 240px" class="news-img"
                                src="<?= Yii::getAlias('@web') . '/images/work/' . $model->id . '/cropped/' . $model->avatar->path ?>"></a>


                    </div>
                </div>
            <?php endif; ?>
            <?php if (!empty($files)): ?>
            <div class="box-body">
                <h3>Документы</h3>
                <?php foreach ($files as $count=> $file): ?>
                    <div>
                        <img  style="width: 40px" src="<?=Yii::getAlias('@web'). '/images/file.png '?>">
                    <a class="btn btn-default" href="<?= Yii::getAlias('@web') . '/images/file/' . $file->work_id . '/' . $file->path ?>" download="<?=  $file->path ?>"> Скачать файл_<?= $count?></a>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php endif; ?>

        </div>

    </div>

</div>
