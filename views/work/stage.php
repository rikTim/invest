<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Work */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="work-form">

    <div class="box box-default">

        <div class="box-body">

            <?php $form = ActiveForm::begin(); ?>



            <?= $form->field($model, 'state_id')->dropDownList($stage, ['id'=>'cat-id','prompt' => 'Выберите ступень']) ?>




            <?= $form->field($model, 'name')->textInput() ?>



        </div>

        <div class="box-footer">
            <div class="form-btn">
                <?= Html::submitButton( 'Создать' , ['class' =>  'btn btn-success']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
