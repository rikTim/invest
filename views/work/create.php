<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Work */

$this->title = 'Create Work';
$this->params['breadcrumbs'][] = ['label' => 'Works', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-create">

    <?= $this->render('_form', [
        'model' => $model,
        'project_id' => $project_id,
        'status' => $status,
        'stage'=> $stage,
        'upload' => $upload,
//        'money' => $money,
        'file' => $file,
        'brigade'=> $brigade,
    ]) ?>

</div>
