<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MaterialMoney */

$this->title = 'Create Material Money';
$this->params['breadcrumbs'][] = ['label' => 'Material Moneys', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-money-create">

    <?= $this->render('_form', [
        'model' => $model,
        'id'=>$id,
        'project_id'=>$project_id,
        'user'=>$user,
    ]) ?>

</div>
