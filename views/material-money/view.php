<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MaterialMoney */

$this->title = 'View Material Money' . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Material Moneys', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-money-view">

    <div class="box">

        <div class="box-header with-border">
            <?php if (Yii::$app->user->can('admin')): ?>
                <?= Html::a('Назад', ['index', 'id' => $model->nomenclatura_id], ['class' => 'btn btn-warning']) ?>
                <?= Html::a('Добавить', ['create', 'id' => $model->nomenclatura_id], ['class' => 'btn btn-success']) ?>
            <?php else: ?>
                <!--                --><? //= Html::a('Назад', ['investor-project/mai','id'=>$model->nomenclatura_id], ['class' => 'btn btn-warning']) ?>
            <?php endif ?>
        </div>
        <!--        --><?php //print_r($model);die(); ?>
<!--        --><?php //$model->nomenclatura_id = $model->nomenclatura->name ?>
        <?php $materialNames = $model->nomenclatura->materialName;
                foreach($materialNames as $materialName){
                    $model->material_name .= '<a href="'.\yii\helpers\Url::to(['fabric-test/view','id'=>$materialName->id]).'">'.$materialName->material->name.'</a>&nbsp;&nbsp;/&nbsp;&nbsp;';
                }
        ?>


        <div class="box-body">
            <?php $model->datetime = date('d.m.Y H:i:s', $model->datetime); ?>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'attribute'=>'nomenclatura_id',
                        'format'=>'html',
                        'label'=>'Номенклатура'
                    ],

                    'datetime',
                    'money',
                    'text:ntext',

                [
                    'attribute'=>'material_name',
                    'format'=>'html',
                    'label'=>'Материалы'
                ],

                ],
            ]) ?>

        </div>

    </div>

</div>
