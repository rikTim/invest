<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FabricTest */

?>
<div class="fabric-test-create">

    <?= $this->render('_form', [
        'model' => $model,
        'id'=>$id,
        'project_id'=>$project_id,
        'material' => $material,

    ]) ?>
</div>
