<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FabricTest */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fabric-test-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'material_id')->dropDownList($material, ['prompt' => 'Виберите материал']) ?>


<!--    --><?//= $form->field($model, 'money_picture')->textInput() ?>

    <?= $form->field($model, 'project_id')->hiddenInput(['value'=>$project_id])->label(false); ?>

<!--    --><?//= $form->field($model, 'is_paid')->textInput() ?>
    <?= $form->field($model, 'price')->textInput(['maxlength' => true,'id'=>'price']) ?>

    <?= $form->field($model, 'number')->textInput(['maxlength' => true,'id'=>'number']) ?>


    <?= $form->field($model, 'money')->textInput(['maxlength' => true, 'id' => 'sum-money','disabled' => 'disabled']) ?>


    <?= $form->field($model, 'nomenclatura_id')->hiddenInput(['value'=>$id])->label(false); ?>
<!--    --><?//= $form->field($model, 'nomenclatura_id')->textInput()->label(false); ?>





	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
<script>
    (function ($) {

        $('#price').on('keyup', function () {
            var price = $('#price').val();
            var number = $('#number').val();
            var sum = price*number;
            $('#sum-money').val(sum.toFixed(2));
            //console.log(1);
        });


        $('#number').on('keyup', function () {
            var price = $('#price').val();
            var number = $('#number').val();
            var sum = price*number;
            $('#sum-money').val(sum.toFixed(2));
            //console.log(1);
        });

    })(jQuery);
</script>