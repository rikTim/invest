<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FabricTest */
?>
<div class="fabric-test-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            [
                'attribute'=>'nomenclatura.project.name',
                'label'=>'Проект',
            ],
            [
                'attribute'=>'nomenclatura.name',
                'label'=>'Накладная'
            ],
            'material.name',

            'money_picture',
//            'kurs',


//            'is_paid',
            'price',
            'number',
            'money',

        ],
    ]) ?>

</div>
