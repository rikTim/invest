<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FabricTest */
?>
<div class="fabric-test-update">

    <?= $this->render('_form', [
        'model' => $model,
        'material'=>$material,
        'project_id'=>$project_id,
        'id'=>$id,
    ]) ?>

</div>
