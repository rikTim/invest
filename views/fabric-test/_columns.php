<?php
use yii\helpers\Url;
use \kartik\grid\GridView;

return [
//    [
//        'class' => 'kartik\grid\CheckboxColumn',
//        'width' => '20px',
//    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'nomenclatura.name',
        'label' => 'Номенклатура',
        'group' => true,
        'groupedRow' => true,
//        'subGroupOf'=> 0,
        'groupOddCssClass' => 'kv-grouped-row',  // configure odd group cell css class
        'groupEvenCssClass' => 'kv-grouped-row',


    ],


    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'nomenclatura.avatar_id',
        'label' => 'Фото подтверждение',
        'group' => true,
        'format' => 'html',
        'value' => function ($data) {
            if (!empty($data->nomenclatura->avatar_id)) {
                return '<a  class="show" data-url='. Yii::getAlias('@web') . '/images/nomenclatura/' . $data->nomenclatura_id . '/' . $data->nomenclatura->avatar->path.' ><img style="width: 240px" src=' . Yii::getAlias('@web') . '/images/nomenclatura/' . $data->nomenclatura_id . '/cropped/' . $data->nomenclatura->avatar->path . '></a>';
            } else return false;
        },
//        'groupedRow'=>  true,
        'subGroupOf' => 0,
        'groupFooter' => function ($model, $key, $index, $widget) { // Closure method
            return [
                'mergeColumns' => [[2, 3]], // columns to merge in summary
                'content' => [              // content to show in each summary cell
                    6 => 'Сумма: ',
                    10 => GridView::F_SUM,
//                    7 => GridView::F_SUM,
//                    8 => GridView::F_SUM,
                    14 => GridView::F_SUM,
                ],
                'contentFormats'=>[      // content reformatting for each summary cell
                    10=>['format'=>'number', 'decimals'=>2],
                    14=>['format'=>'number', 'decimals'=>2],
                ],
                'contentOptions'=>[      // content html attributes for each summary cell
                    10=>['style'=>'text-align:right'],
                    14=>['style'=>'text-align:right'],
                ],


                // html attributes for group summary row
                'options' => ['class' => 'success', 'style' => 'font-weight:bold;']
            ];
        },
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'nomenclatura.provider',
        'label' => 'Поставщик',
        'group' => true,
//        'groupedRow'=>  true,
        'subGroupOf' => 0,
    ],

    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'nomenclatura.stage.name',
        'label' => 'Этап',
        'group' => true,
//        'groupedRow'=>  true,
        'subGroupOf' => 0,
        'content'=> function($data){
            if($data->nomenclatura->stage_id == 3)
            {
                return 'Ступень № 3,4  - Коробка';
            }
            else {
                return $data->nomenclatura->stage->name;
            }
        }
    ],

    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'nomenclatura.datetime',
        'label' => 'Дата',
        'group' => true,
        'format' => 'date',
//        'groupedRow'=>  true,
        'subGroupOf' => 0,
    ],


    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Статус',
        'attribute' => 'nomenclatura.status.name',
        'group' => true,
        'subGroupOf' => 0,
    ],
    [
        'class' => '\kartik\grid\DataColumn',

        'attribute' => '1',
        'group' => true,
        'format' => 'html',
        'content' => function ($data) {
            $url = \yii\helpers\Url::toRoute(['fabric-test/create', 'id' => $data->nomenclatura->id]);

            return \yii\helpers\Html::a('<span class="glyphicon glyphicon-plus"></span>&nbsp;', $url, [
                'role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'
            ]);
        },
//        material-money/index','id'=>$model->id
//        'groupedRow'=>  true,
        'subGroupOf' => 0,
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => '2',
        'group' => true,
        'format' => 'html',
        'content' => function ($data) {
            if(round($data->nomenclatura->sum,2) < round($data->nomenclatura->material,2)) {
                $url = \yii\helpers\Url::toRoute(['material-money/index', 'id' => $data->nomenclatura->id]);
                return \yii\helpers\Html::a('<span class="glyphicon glyphicon-usd"></span>&nbsp;', $url, [
                    'title' => 'Update'
                ]);
            }else{
                return false;
            }
        },
//        'groupedRow'=>  true,
        'subGroupOf' => 0,
    ],

    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => '3',
        'group' => true,
        'format' => 'html',
        'content' => function ($data) {
            if(Yii::$app->user->can('admin') && $data->nomenclatura->status_id==1) {
                $url = \yii\helpers\Url::toRoute(['fabric-test/success', 'id' => $data->nomenclatura->id]);
                return \yii\helpers\Html::a('<span class="glyphicon glyphicon-ok"></span>&nbsp;', $url, [
                    'title' => 'Success'
                ]);
            }else{
                return false;
            }
        },
//        'groupedRow'=>  true,
        'subGroupOf' => 0,
    ],

    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => '4',
        'group' => true,
        'format' => 'html',
        'content' => function ($data) {
//            if(Yii::$app->user->can('admin') && $data->nomenclatura->status_id==1) {
                $url = \yii\helpers\Url::toRoute(['nomenclatura/update', 'id' => $data->nomenclatura->id]);
                return \yii\helpers\Html::a('<span class="glyphicon glyphicon-pencil"></span>&nbsp;', $url, [
                    'title' => 'Success'
                ]);
//            }else{
//                return false;
//            }
        },
//        'groupedRow'=>  true,
        'subGroupOf' => 0,
    ],


    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'nomenclatura.sum',
        'label' => 'Оплаченая Сумма',
        'group' => true,
        'contentOptions'=>[
            'style'=>'text-align:right'
        ],
        'content'=>function($data){
          return round($data->nomenclatura->sum,2);
        },
//        'format' => 'datetime',
//        'groupedRow'=>  true,
        'subGroupOf' => 0,
//                'format' => ['decimal', 2],
        'pageSummary' => true,
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'material.name',
        'label' => 'Материал',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Цена за единицу',
        'attribute' => 'price',
        'contentOptions'=>[
            'style'=>'text-align:right'
        ],

    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Количество',
        'attribute' => 'number',
        'contentOptions'=>[
            'style'=>'text-align:right'
        ],
    ],

    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Стоимость',
        'attribute' => 'money',
        'contentOptions'=>[
            'style'=>'text-align:right'
        ],
//        'format' => ['decimal', 2],
        'pageSummary' => true,
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'money_picture',
//        'label'=>'Фото потверждение',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'kurs',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'project_id',
//    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'is_paid',
    // ],

    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'nomenclatura_id',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'template' => '{view} {update} {delete}',
        'visibleButtons' => [
            'update' => function ($model, $key, $index) {
                return $model->nomenclatura->status_id == 1;
            },
            'delete' => function ($model, $key, $index) {
                return $model->nomenclatura->status_id == 1;
            },
        ],
//        'buttons' => [
//            'create' => function ($url, $model) {
//                return \yii\helpers\Html::a('<span class="glyphicon glyphicon-plus"></span>&nbsp;', $url, [
//                    'role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'
//                ]);
//            },
//
//
//        ],
//        'urlCreator' => function ($action, $model, $key, $index) {
//            switch($action){
//                case 'create':
//                    $url = \yii\helpers\Url::toRoute(['fabric/create','id'=>$model->id]);
//                    return $url;
//                case 'update':
//                    $url = \yii\helpers\Url::toRoute(['fabric-test/update','id'=>$model->id]);
//                    return $url;
//                case 'view':
//                    $url = \yii\helpers\Url::toRoute(['fabric-test/view','id'=>$model->id]);
//                    return $url;
//                case 'delete':
//                    $url = \yii\helpers\Url::toRoute(['fabric-test/delete','id'=>$model->id]);
//                    return $url;
//
//
//            }
//
//
//        },

        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
//        'createOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'],
//        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Delete',
//            'data-confirm' => false, 'data-method' => false,// for overide yii data api
//            'data-request-method' => 'post',
//            'data-toggle' => 'tooltip',
//            'data-confirm-title' => 'Are you sure?',
//            'data-confirm-message' => 'Are you sure want to delete this item'],
    ],

];   