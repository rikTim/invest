<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Investor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="investor-form">

	<div class="box box-default">

		<div class="box-body">

	    <?php $form = ActiveForm::begin(); ?>

	    <?= $form->field($model, 'investor_id')->dropDownList($user) ?>

    <?= $form->field($model, 'project_id')->hiddenInput(['value'=>$id])->label(false) ?>

    <?= $form->field($model, 'persent')->textInput(['maxlength' => true]) ?>

		</div>

	    <div class="box-footer">
			<div class="form-btn">
	        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    	</div>
	    </div>

    <?php ActiveForm::end(); ?>

    </div>

</div>
