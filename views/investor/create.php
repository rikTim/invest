<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Investor */

$this->title = 'Create Investor';
$this->params['breadcrumbs'][] = ['label' => 'Investors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="investor-create">

    <?= $this->render('_form', [
        'model' => $model,
        'id'=>$id,
        'user'=>$user,
    ]) ?>

</div>
