<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InvestorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Investors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="investor-index">
    <center><h2><?= $model->name?></h2></center>
    <div class="box">

        <div class="box-header with-border">
            <div class="pull-left">
                <?= Html::a('Добавить инвестора', ['create', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
            </div>
            <div class="pull-right">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
        </div>

        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
//                    ['class' => 'yii\grid\SerialColumn'],

//                    'id',
                [
                    'attribute'=>'investor_id',
                    'value'=>function($data){
                        return $data->investor->name;
                    }
                ],
//                    'investor.name',
//                    'project_id',
                    'persent',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '70'],
                        'template' => '{view} ',
                    ],
                ],
            ]); ?>
        </div>

    </div> <!--end box -->

</div>
