<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Investor */

$this->title = 'View Investor' . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Investors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="investor-view">

    <div class="box">

        <div class="box-header with-border">
        <?= Html::a('Назад', ['index','id'=>$model->project_id], ['class' => 'btn btn-warning']) ?>
        <?= Html::a('Создать', ['create','id'=>$model->project_id], ['class' => 'btn btn-success']) ?>
<!--        --><?//= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
<!--        --><?//= Html::a('Delete', ['delete', 'id' => $model->id], [
//            'class' => 'btn btn-danger',
//            'data' => [
//                'confirm' => 'Are you sure you want to delete this item?',
//                'method' => 'post',
//            ],
//        ]) ?>

        </div>

        <div class="box-body">
<?php
$model->investor_id= $model->investor->name;
$model->project_id = $model->project->name;
?>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
//                'id',
            'investor_id',
            'project_id',
            'persent',
            ],
        ]) ?>

        </div>

    </div>

</div>
