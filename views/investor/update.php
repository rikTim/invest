<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Investor */

$this->title = 'Update Investor' . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Investors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="investor-update">

    <?= $this->render('_form', [
        'model' => $model,
        'user' => $user,
        'id' => $id,
    ]) ?>

</div>
