<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WorkComment */

$this->title = 'Update Work Comment' . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Work Comments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="work-comment-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
