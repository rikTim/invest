<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\WorkCommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Work Comments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-comment-index">

    <div class="box">

        <div class="box-header with-border">
            <div class="pull-left">
                <?php if(Yii::$app->user->can('admin') || Yii::$app->user->can('moder')):?>
                <?= Html::a('Добавить примечание', ['create','id'=>$id], ['class' => 'btn btn-success']) ?>
                <?php endif;?>
            </div>
            <div class="pull-right">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                    </div>
        </div>

        <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'work.work.name',
            'text:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '70'],
                'template' => '{view}',
            ],
        ],
    ]); ?>
        </div>

    </div> <!--end box -->

</div>
