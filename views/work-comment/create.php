<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\WorkComment */

$this->title = 'Create Work Comment';
$this->params['breadcrumbs'][] = ['label' => 'Work Comments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-comment-create">

    <?= $this->render('_form', [
        'model' => $model,
        'id'=>$id
    ]) ?>

</div>
