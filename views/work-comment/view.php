<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\WorkComment */

$this->title = 'View Work Comment' . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Work Comments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-comment-view">

    <div class="box">

        <div class="box-header with-border">
            <?= Html::a('Главная', ['index', 'id' => $model->work_id], ['class' => 'btn btn-warning']) ?>
            <?php if (Yii::$app->user->can('admin') || Yii::$app->user->can('moder')): ?>
                <?= Html::a('Добавить', ['create', 'id' => $model->work_id], ['class' => 'btn btn-success']) ?>
            <?php endif; ?>
            <!--        --><? //= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <!--        --><? //= Html::a('Delete', ['delete', 'id' => $model->id], [
            //            'class' => 'btn btn-danger',
            //            'data' => [
            //                'confirm' => 'Are you sure you want to delete this item?',
            //                'method' => 'post',
            //            ],
            //        ]) ?>

        </div>

        <div class="box-body">
            <?php $model->work_id = $model->work->work->name ?>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
//                'id',
                    'work_id',
                    'text:ntext',
                ],
            ]) ?>

        </div>

    </div>

</div>
