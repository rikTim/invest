<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Material */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="material-form">

    <div class="box box-default">

        <div class="box-body">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>

            <?= $form->field($upload, 'imageFile')->fileInput()->label(false) ?>

            <?= $form->field($model, 'units')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'per_unit')->textInput(['maxlength' => true]) ?>

        </div>

        <div class="box-footer">
            <div class="form-btn">
                <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>


        <?php ActiveForm::end(); ?>


        <?php if (!$model->isNewRecord && !empty($model->photo_id)): ?>
            <div class="box-body">
                <div id='gallery'>
                    <a href="<?= Yii::getAlias('@web') . '/images/materialAvatar/' . $model->id . '/' . $model->photo->path ?> "><img
                            style="width: 240px" class="news-img"
                            src="<?= Yii::getAlias('@web') . '/images/materialAvatar/' . $model->id . '/cropped/' . $model->photo->path ?>"></a>
                </div>
            </div>
        <?php endif; ?>


    </div>

</div>
