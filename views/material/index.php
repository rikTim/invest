<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Materials';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-index">

    <div class="box">

        <div class="box-header with-border">
            <div class="pull-left">
                <?= Html::a('Добавить материал', ['create'], ['class' => 'btn btn-success']) ?>
            </div>
            <div class="pull-right">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                    </div>
        </div>

        <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'name',
            'number',
            [
                'attribute'=>'photo_id',
                'format'=>'raw',
                'value'=>function($data){
                    if(!empty($data->photo_id)) {
                        return '<img
                            style="width: 240px" class="news-img"
                            src="'.Yii::getAlias('@web') . '/images/materialAvatar/' . $data->id . '/' . $data->photo->path.'">';
                    }else return false;
                }
            ],
            'photo_id',
            'units',
            // 'per_unit',

            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '70'],
                'template' => '{view} {update}',
            ],
        ],
    ]); ?>
        </div>

    </div> <!--end box -->

</div>
