<?php


use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <h2>
            Пожалуйста, заполните следующие поля , чтобы зарегистрироваться:</h2>
    </div>
    <div class="row">

        <div class="col-lg-11">


            <?php $form = ActiveForm::begin([
                'enableClientValidation' => true,
                'options' => [
                    'enctype' => 'multipart/form-data',
                ],
            ]) ?>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <?= $form->field($model, 'name') ?>

                    <?= $form->field($model, 'email') ?>
                    <?= $form->field($model, 'password')->passwordInput() ?>


                </div>


                <div class="col-md-12 text-center save-btn">
                    <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
