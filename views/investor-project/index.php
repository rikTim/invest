<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MaterialMoneySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Material Moneys';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-money-index">

    <div class="box">
        <!---->
        <!--        <div class="box-header with-border">-->
        <!--            <div class="pull-left">-->
        <!--                --><? //= Html::a('Внести оплату', ['create','id'=>$id], ['class' => 'btn btn-success']) ?>
        <!--            </div>-->
        <!--            <div class="pull-right">-->
        <!--                --><?php //echo $this->render('_search', ['model' => $searchModel]); ?>
        <!--            </div>-->
        <!--        </div>-->
<!--<button id="gallery"></button>-->
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [

                    [
                        'attribute' => 'nomenclatura.avatar_id',
                        'label' => 'Фото подтверждение',
                        'format' => 'html',
                        'content' => function ($data) {
                            if($data->type==2) {
                                if (!empty($data->nomenclatura->avatar_id)) {
                                    return '<a data-url="'. Yii::getAlias('@web') . '/images/nomenclatura/' . $data->nomenclatura_id . '/' . $data->nomenclatura->avatar->path.'" class="show"><img style="width: 240px" src=' . Yii::getAlias('@web') . '/images/nomenclatura/' . $data->nomenclatura_id . '/cropped/' . $data->nomenclatura->avatar->path . '></a>';
//                                    return '<button id="gallery" data-url="'. Yii::getAlias('@web') . '/images/nomenclatura/' . $data->nomenclatura_id . '/' . $data->nomenclatura->avatar->path.'"></button>';
                                } else return false;
                            }
                            else{
                                if(!empty($data->work->money_picture)){


                                    return '<a class="show" " data-url="'. Yii::getAlias('@web') . '/images/work/' . $data->work_id . '/' . $data->work->avatar->path.'"><img style="width: 240px" src=' . Yii::getAlias('@web') . '/images/work/' . $data->work_id . '/cropped/' . $data->work->avatar->path . '></a>';
                                }
                            }
                        },
                    ],

                    [
                        'attribute' => 'nomenclatura.name',
                        'label' => 'Название',
                        'content' => function ($data) {
                            if (($data->type == 2)) {
                                return $data->nomenclatura->name;
                            } else {
                                return $data->work->work->name;
                            }
                        }

                    ],




                    [
                        'attribute' => 'nomenclatura_id',
                        'format' => 'html',
                        'label' => 'Материалы',
                        'content' => function ($data) {
                            if ($data->type == 2) {
                                return \app\models\Nomenclatura::getMaterialNames($data->nomenclatura_id);
                            }
                            else{
                                return false;
                            }
                        }
                    ],

                    [
                        'attribute' => 'datetime',
                        'filter' => \yii\jui\DatePicker::widget(['model' => $searchModel, 'attribute' => 'datetime',
                            'language' => 'ru',
                            'dateFormat' => 'yyyy-MM-dd',
                            'options' => ['class' => 'form-control'],
                        ]),
                        'content' => function ($data) {
                            if (!empty($data->datetime)) {
                                return date('d.m.Y H:i:s', $data->datetime);
                            }
                        }
                    ],
                    'money',
                    'text:ntext',

                    [
                        'attribute' => 'type',
                        'label' => 'Тип оплаты',
                        'content' => function ($data) {
                            if (($data->type == 2)) {
                                return 'Материалы';
                            } else {
                                return 'Работы';
                            }
                        }

                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '70'],
                        'template' => '{view}',

                        'urlCreator' => function ($action, $model, $key, $index) {
                            switch ($action) {
                                case 'view':
                                    if ($model->type == 2) {

                                        $url = \yii\helpers\Url::toRoute(['material-money/view', 'id' => $model->id]);
                                    } else {
                                        $url = \yii\helpers\Url::toRoute(['work-money/view', 'id' => $model->id]);
                                    }

                                    return $url;
                            }
                        }
                    ],
                ],
            ]); ?>
        </div>

    </div> <!--end box -->

</div>
<!--<script>-->
<!--    $('#gallery').click(function () {-->
<!--        console.log(1);-->
<!--       var url =  $('#gallery').data("url");-->
<!--        console.log(url);-->
<!--        var modalContainer = $('#galleryShow');-->
<!--        //var modalBody = modalContainer.find('.modal-body');-->
<!--        modalContainer.modal({show: true});-->
<!--    });-->
<!--</script>-->
