<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\InvestorProject */

$this->title = 'View Investor Project' . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Investor Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="investor-project-view">

    <div class="box">

        <div class="box-header with-border">
            <?= Html::a('Главная', ['index', 'id' => $model->investor_id], ['class' => 'btn btn-warning']) ?>
            <?php if (Yii::$app->user->can('admin')): ?>
                <?= Html::a('Добавить', ['create', 'id' => $model->investor_id], ['class' => 'btn btn-success']) ?>
<!--                --><?//= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?php endif; ?>


        </div>

        <div class="box-body">
            <?php $model->datetime = date('d.m.Y H:i:s', $model->datetime) ?>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'investor.name',
                    'project.name',
                    'money',
                    'datetime',
                ],
            ]) ?>

        </div>

    </div>

</div>
