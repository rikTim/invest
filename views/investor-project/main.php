<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Investor;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <div class="box">

        <div class="box-header with-border">
            <div class="pull-left">
<!--                --><?//= Html::a('Добавить пользователя', ['create'], ['class' => 'btn btn-success']) ?>
            </div>
            <div class="pull-right">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
        </div>

        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'name',
//                    'username',
//                    'email:email',
//                    'status',

                    [
                        'attribute'=>'sum',
                        'label'=>'Инвестиции',
                        'value'=>function($data){
                            return   Investor::investion($data->id);
                        }
                    ],
                    [
                        'attribute'=>'minus',
                        'label'=>'Потрачено',
                        'value'=>function($data){
                            return round(Investor::materialMoney($data->id) + Investor::workMoney($data->id),2);
                        }
                    ],


                    [
                        'attribute'=>'minus',
                        'label'=>'Остаток в кассе',
                        'value'=>function($data){
                            return Investor::sumMoney($data->id);
                        }
                    ],


//                    'role.item_name',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '70'],
                        'template' => '{money}',

                        'buttons' => [
                            'money' => function ($url, $model) {
                                return Html::a('&nbsp;&nbsp;<span class="glyphicon glyphicon-usd"></span>', $url, [
                                    'title' => Yii::t('app', 'Work'),
                                ]);
                            },



                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            switch($action){
                                case 'money':
                                    $url = \yii\helpers\Url::toRoute(['investor-project/index','id'=>$model->id]);
                                    return $url;
                            }
                        }

                    ],
                ],
            ]); ?>
        </div>

    </div> <!--end box -->

</div>
