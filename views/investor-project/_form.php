<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\InvestorProject */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="investor-project-form">

    <div class="box box-default">

        <div class="box-body">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'investor_id')->hiddenInput(['value' => $id])->label(false) ?>

            <?= $form->field($model, 'project_id')->dropDownList(['promt'=>'Выберите обьект']+$project) ?>

            <?= $form->field($model, 'money')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'kurs')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'datetime')->hiddenInput(['value' => time()])->label(false) ?>

        </div>

        <div class="box-footer">
            <div class="form-btn">
                <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
