<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\InvestorProject */

$this->title = 'Create Investor Project';
$this->params['breadcrumbs'][] = ['label' => 'Investor Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="investor-project-create">

    <?= $this->render('_form', [
        'model' => $model,
        'id'=>$id,
        'project'=>$project,
    ]) ?>

</div>
