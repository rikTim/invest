<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\WorkersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список Работников';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="workers-index">

    <div class="box">

        <div class="box-header with-border">
            <div class="pull-left">
                <?= Html::a('Добавить Работника', ['create'], ['class' => 'btn btn-success']) ?>
            </div>
            <div class="pull-right">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
        </div>

        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [

                    'name',
                    'lastname',
                    [
                        'attribute' => 'photo_id',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return '<img src=' . Yii::getAlias('@web') . '/images/workers/' . $data->id . '/cropped/' . $data->avatar->path . '>';
                        }
                    ],

//                    [
//                        'attribute' => 'id',
//                        'label' => 'Бригада',
//                        'format' => 'raw',
//                        'value' => function ($data) {
//                            $name = 'Нет';
//                            if ($data->team != NULL) {
//                                $name = $data->team->brigade->name;
//                            }
//                            return $name;
//                        }
//                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '70'],
                        'template' => '{view} {update} ',
                    ],
                ],
            ]); ?>
        </div>

    </div>

</div>
