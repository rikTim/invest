<?php

namespace app\controllers;

use app\models\Brigade;
use app\models\File;
use app\models\Material;
use app\models\MaterialMoney;
use app\models\Status;
use app\models\UploadForm;
use Yii;
use app\models\Fabric;
use app\models\FabricSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * FabricController implements the CRUD actions for Fabric model.
 */
class FabricController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Fabric models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $searchModel = new FabricSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'id' => $id
        ]);
    }

    /**
     * Displays a single Fabric model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $files = File::find()->where(['fabric_id' => $id])->all();

        return $this->render('view', [
            'model' => $this->findModel($id),
            'files' => $files
        ]);
    }

    /**
     * Creates a new Fabric model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($project_id)
    {
        $model = new Fabric();
        $money = new MaterialMoney();
        if ($model->load(Yii::$app->request->post())) {
            $model->money = $model->price* $model->number;
            $model->save();
            $money->material_id = $model->id;
            $money->project_id = $model->project_id;
            $money->kurs =  $model->kurs;
            $money->save();

        } else {
            return $this->render('create', [
                'model' => $model,
                'id' => $project_id,
                'status' => ArrayHelper::map(Status::find()->all(), 'id', 'name'),
                'brigade' => ArrayHelper::map(Brigade::find()->all(), 'id', 'name'),
                'material' => ArrayHelper::map(Material::find()->all(), 'id', 'name'),
                'money' => $money,

            ]);
        }
    }

    /**
     * Updates an existing Fabric model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->money = $model->price* $model->number;
            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'id' => $model->project_id,
                'status' => ArrayHelper::map(Status::find()->all(), 'id', 'name'),
                'brigade' => ArrayHelper::map(Brigade::find()->all(), 'id', 'name'),
                'material' => ArrayHelper::map(Material::find()->all(), 'id', 'name'),

            ]);
        }
    }

    /**
     * Deletes an existing Fabric model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Fabric model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Fabric the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Fabric::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }




}
