<?php

namespace app\controllers;

use app\models\AuthAssignment;
use app\models\Photo;
use app\models\Stage;
use app\models\Status;
use app\models\UploadForm;
use app\models\File;
use Yii;
use app\models\Project;
use app\models\ProjectSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProjectController implements the CRUD actions for Project model.
 */
class ProjectController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),

                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],

                    ],

                    [
                        'actions' => ['index','create','update','image','view','file'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'actions' => ['image','view','file'],
                        'allow' => true,
                        'roles' => ['investor'],
                    ],

                ],

            ],

        ];
    }

    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    }

    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $files = File::find()->where(['project_id' => $id,'type'=>1])->all();
        $photos = Photo::find()->where(['project_id'=> $id,'type'=>3])->all();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'photos' =>$photos,
            'files'=> $files
        ]);
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Project();
        $upload = new UploadForm();
        $upload->scenario = 'project';

//        $upload->scenario = 'create';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            $upload->avatar($model->id);


            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'status' => ArrayHelper::map(Status::find()->all(), 'id', 'name'),
                'upload' => $upload,


            ]);
        }
    }

    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $upload = new UploadForm();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            if (!empty($upload->imageFile)) {

                $upload->avatar($model->id);
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'status' => ArrayHelper::map(Status::find()->all(), 'id', 'name'),
                'upload' => $upload,
            ]);
        }
    }

    /**
     * Deletes an existing Project model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
//    public function actionDelete($id)
//    {
//        $this->findModel($id)->delete();
//
//        return $this->redirect(['index']);
//    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionImage($id)
    {
        $model = $this->findModel($id);
        $photoModel = new Photo();
        $upload = new UploadForm();

        $photos = Photo::find()->where(['project_id'=> $id,'type'=>3])->all();

        if ( $photoModel->load(Yii::$app->request->post()) ) {
            $upload->files = UploadedFile::getInstances($upload, 'files');
            if (!empty($upload->files)) {
                $upload->photos($photoModel->stage_id,$model->id);
            }
            return $this->redirect(['index']);
        }

        return $this->render('image', [
            'model' => $model,
            'photoModel' =>$photoModel,
            'upload' => $upload,
            'photos' =>$photos,
            'stage' => ArrayHelper::map(Stage::find()->all(), 'id', 'name'),
        ]);
    }

    public function actionFile($id)
    {
        $model = $this->findModel($id);
        $upload = new UploadForm();
        $upload->scenario = 'documents';

        $files = File::find()->where(['project_id'=>$id,'type'=>1])->all();
        if ( !empty($_POST) ) {

            $upload->files = UploadedFile::getInstances($upload, 'files');
            if (!empty($upload->files)) {

                $upload->projectDocuments($id);
            }
            return $this->redirect(['index']);
        }
        return $this->render('file', [
            'upload' => $upload,
            'files' =>$files,
            'model'=>$model
        ]);
    }
}
