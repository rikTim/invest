<?php

namespace app\controllers;

use app\models\AuthAssignment;
use app\models\Fabric;
use app\models\Investor;
use app\models\Material;
use app\models\MaterialMoney;
use app\models\Nomenclatura;
use app\models\UploadForm;
use Yii;
use app\models\FabricTest;
use app\models\FabricTestSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * FabricTestController implements the CRUD actions for FabricTest model.
 */
class FabricTestController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                    'bulk-create' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all FabricTest models.
     * @return mixed
     */
    public function actionIndex($id)
    {    
        $searchModel = new FabricTestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'project_id'=>$id,
        ]);
    }


    /**
     * Displays a single FabricTest model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Материал #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new FabricTest model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $request = Yii::$app->request;
        $nomenklaturaModel = Nomenclatura::findOne($id);
        $model = new FabricTest();



//        $model->scenario = 'create';
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить новый материал",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'id'=>$id,
                        'project_id'=>$nomenklaturaModel->project_id,
                        'material' => ArrayHelper::map(Material::find()->addOrderBy('name')->all(), 'id', 'name'),

                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) ){
                $model->money = $model->price* $model->number;
                $model->save();

                $is_null = FabricTest::find()->where(['nomenclatura_id'=>$model->nomenclatura_id,'type'=>'create'])->one();
                if(!empty($is_null))
                {
                    $is_null->delete();
                }
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Добавить новый материал",
                    'content'=>'<span class="text-success">Создано Успешно</span>',
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Добавить еще',['create','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Добавить новый материал",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'id'=>$id,
                        'project_id'=>$nomenklaturaModel->project_id,
                        'material' => ArrayHelper::map(Material::find()->all(), 'id', 'name'),

                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Добавить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'id'=>$id,
                    'project_id'=>$nomenklaturaModel->project_id,
                    'material' => ArrayHelper::map(Material::find()->all(), 'id', 'name'),
                ]);
            }
        }
       
    }

    /**
     * Updates an existing FabricTest model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
//        $nomenklaturaModel = Nomenclatura::findOne($id);

//        $model->scenario = 'create';
        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить материал #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'material' => ArrayHelper::map(Material::find()->all(), 'id', 'name'),
                        'project_id'=>$model->project_id,
                        'id'=>$model->nomenclatura_id,

                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post())){
                $model->money = $model->price* $model->number;
                $model->save();
//                print_r($model->getErrors());
//                die();


                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Материал #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Изменить материал #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'material' => ArrayHelper::map(Material::find()->all(), 'id', 'name'),
                        'project_id'=>$model->project_id,
                        'id'=>$model->nomenclatura_id,

                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Добавить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'project_id'=>$model->project_id,
                    'material' => ArrayHelper::map(Material::find()->all(), 'id', 'name'),
                    'id'=>$id,
                ]);
            }
        }
    }

    /**
     * Delete an existing FabricTest model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing FabricTest model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    public function actionBulkCreate()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $request = Yii::$app->request;
            $model = new FabricTest();
            $model->scenario = 'create';
            if($request->isAjax){
                /*
                *   Process for ajax request
                */
                Yii::$app->response->format = Response::FORMAT_JSON;
                if($request->isGet){
                    return [
                        'title'=> "Create new FabricTest",
                        'content'=>$this->renderAjax('create', [
                            'model' => $model,
                        ]),
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                    ];
                }else if($model->load($request->post()) && $model->save()){
                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'title'=> "Create new FabricTest",
                        'content'=>'<span class="text-success">Create FabricTest success</span>',
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                    ];
                }else{
                    return [
                        'title'=> "Create new FabricTest",
                        'content'=>$this->renderAjax('create', [
                            'model' => $model,
                        ]),
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                    ];
                }
            }else{
                /*
                *   Process for non-ajax request
                */
                if ($model->load($request->post()) && $model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }
            }
        }

    }

    /**
     * Finds the FabricTest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FabricTest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FabricTest::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionSuccess($id)
    {
        $request = Yii::$app->request;
        $materialModel = Nomenclatura::findOne($id);
        $project_id = $materialModel->project_id;
        $materialModel->datetime = date('d.m.Y H:i:s', $materialModel->datetime);
        $materialModel->stage_id = $materialModel->stage->name;
        $materialModel->status_id = $materialModel->status->name;
        $materialModel->project_id = $materialModel->project->name;
        $work_moneys = MaterialMoney::find()->where(['nomenclatura_id' => $id])->all();
        $money = 0;
        foreach ($work_moneys as $work_money) {
            $money += $work_money->money;
        }
        $investors = AuthAssignment::find()->where(['item_name' => 'investor'])->all();
        $user = [];
        foreach ($investors as $investor) {
            if ($investor->user->status == 10) {
                $sum = Investor::sumMoney($investor->user_id);
                $user [$investor->user_id] = $investor->user->name.'('.$sum.')';
            }
        }
        $model = new MaterialMoney();
        if ($model->load($request->post()) ) {
            $model->nomenclatura_id= $id;
            $model->money = round($model->money,2);
            $model->save();
            $materialModel = Nomenclatura::findOne($id);
            $materialModel->status_id = 2;
            $materialModel->save();
            return $this->redirect(['index', 'id' =>  $materialModel->project_id]);
        } else {
            return $this->render('success', [
                'model' => $model,
                'id' => $id,
                'materialModel' =>  $materialModel,
                'money' => $money,
                'project_id'=>$project_id,
                'user'=>$user,
            ]);
        }


    }
}
