<?php

namespace app\controllers;

use app\models\AuthAssignment;
use app\models\Investor;
use app\models\Nomenclatura;
use Yii;
use app\models\MaterialMoney;
use app\models\MaterialMoneySearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MaterialMoneyController implements the CRUD actions for MaterialMoney model.
 */
class MaterialMoneyController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),

                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],

                    [
                        'actions' => ['index','create','update','view','delete'],
                        'allow' => true,
                        'roles' => ['admin','operatorMaterial'],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['investor'],
                    ],
                ],
            ],

        ];
    }

    /**
     * Lists all MaterialMoney models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $searchModel = new MaterialMoneySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'id'=>$id,
        ]);
    }

    /**
     * Displays a single MaterialMoney model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MaterialMoney model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new MaterialMoney();
        $nomenclatura = Nomenclatura::findOne($id);
        $investors = AuthAssignment::find()->where(['item_name' => 'investor'])->all();
        $user = [];
        foreach ($investors as $investor) {
            if ($investor->user->status == 10) {
                $sum = Investor::sumMoney($investor->user_id);
                $user [$investor->user_id] = $investor->user->name.'('.$sum.')';
            }
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'id' =>$id,
                'project_id'=>$nomenclatura->project_id,
                'user'=>$user
            ]);
        }
    }

    /**
     * Updates an existing MaterialMoney model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
//        $model->scenario = 'update';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'id'=>$model->nomenclatura_id,
            ]);
        }
    }

    /**
     * Deletes an existing MaterialMoney model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MaterialMoney model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MaterialMoney the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MaterialMoney::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
