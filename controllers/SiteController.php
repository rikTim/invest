<?php

namespace app\controllers;

use app\models\SignupForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
//     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),

                'rules' => [
                    [
                        'actions' => ['logout','index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['login','index'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {

        if (!\Yii::$app->user->isGuest) {
            if(Yii::$app->user->can('admin')) {
                return $this->redirect(['project/index']);
            }
            if(Yii::$app->user->can('investor')) {
                return $this->redirect(['finance/main']);
            }
            if(Yii::$app->user->can('moder')) {
                return $this->redirect(['work/main']);
            }
            if(Yii::$app->user->can('operatorWork')){
                return $this->redirect(['work/main']);
            }
            if(Yii::$app->user->can('operatorMaterial')){
                return $this->redirect(['work/main']);
            }

        }
        return $this->redirect(['login']);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            if(Yii::$app->user->can('admin')) {
                return $this->redirect(['project/index']);
            }
            if(Yii::$app->user->can('investor')) {
                return $this->redirect(['finance/main']);
            }
            if(Yii::$app->user->can('moder')) {
                return $this->redirect(['work/main']);
            }
            if(Yii::$app->user->can('operatorWork')){
                return $this->redirect(['work/main']);
            }
            if(Yii::$app->user->can('operatorMaterial')){
                return $this->redirect(['work/main']);
            }

        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            if(Yii::$app->user->can('admin')) {
                return $this->redirect(['project/index']);
            }
            if(Yii::$app->user->can('investor')) {
                return $this->redirect(['finance/main']);
            }
            if(Yii::$app->user->can('moder')) {
                return $this->redirect(['work/main']);
            }
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSignup()
    {
        $model = new SignupForm();
//        $avatarUpload = new UploadForm();
//        $avatarUpload->scenario = 'avatarFile';
        if ($model->load(Yii::$app->request->post())) {

            if ($user = $model->signup()) {
//                $avatarUpload->avatarFile = UploadedFile::getInstance($avatarUpload, 'avatarFile');
//                if (!empty($avatarUpload->avatarFile)) {
//                    $user->setAttribute('avatar', $avatarUpload->userUpload($user->id));
//                    $user->save(false);
//                }
////                print_r($user);
////                die();
//                if ($user->status == 5) {
//                    Yii::$app->session->setFlash('warning', 'Спасибо за регистрацию. Ожидайте подтверждение вашего профиля менеджером ');
//                    return $this->goHome();
//                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
//            'cities' => Cities::getList(),
//            'avatarUpload' => $avatarUpload,
        ]);
    }
}
