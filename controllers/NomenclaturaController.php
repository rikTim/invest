<?php

namespace app\controllers;

use app\models\Fabric;
use app\models\FabricTest;
use app\models\Stage;
use app\models\Status;
use app\models\UploadForm;
use Yii;
use app\models\Nomenclatura;
use app\models\NomenclaturaSeach;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * NomenclaturaController implements the CRUD actions for Nomenclatura model.
 */
class NomenclaturaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Nomenclatura models.
     * @return mixed
     */
    public function actionIndex($id)
    {    
        $searchModel = new NomenclaturaSeach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Nomenclatura model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Nomenclatura #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Nomenclatura model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($project_id)
    {
        $request = Yii::$app->request;
        $model = new Nomenclatura();
        $upload = new UploadForm();
        $file = new UploadForm();
//        $upload->scenario = 'create';
        $file->scenario = 'documents';

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить  Накладную",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'project_id'=>$project_id,
                        'status' => ArrayHelper::map(Status::find()->all(), 'id', 'name'),
                        'upload' => $upload,
                        'file' => $file,
                        'stage' => ArrayHelper::map(Stage::find()->where('id != 3 AND id != 4')->all(), 'id', 'name'),
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) ){
                $model->datetime = strtotime($model->datetime);
                $model->save();
                $model = Nomenclatura::findOne( $model->id);
                $model->name = 'Накладная №'.$model->id.' за ' .date('d.m.Y ', $model->datetime);
                $model->save();

                $modelFabric = new  FabricTest();
                $modelFabric->scenario ='create_nomenklatura';
                $modelFabric->project_id = $model->project_id;
                $modelFabric->nomenclatura_id = $model->id;
                $modelFabric->type = 'create';
                $modelFabric->save();

                $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
                $upload->material($model->id, $model->project_id);
                $file->files = UploadedFile::getInstances($file, 'files');
                $file->materialDocuments($model->id, $model->project_id);

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Добавить  Накладную",
                    'content'=>'<span class="text-success">Накладная добавлена</span>',
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::a('Добавить Еще',['create','project_id'=>$project_id],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Добавить  Накладную",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'project_id'=>$project_id,
                        'status' => ArrayHelper::map(Status::find()->all(), 'id', 'name'),
                        'upload' => $upload,
                        'file' => $file,
                        'stage' => ArrayHelper::map(Stage::find()->where('id != 3 AND id != 4')->all(), 'id', 'name'),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) ) {
                $model->datetime = strtotime($model->datetime);
                $model->save();
//                print_r($model);
//                print_r($model->getErrors());die();

                $model = Nomenclatura::findOne( $model->id);
                $model->name = 'Номенклатура №'.$model->id.' за ' .date('d.m.Y ', $model->datetime);
                $model->save();

                $modelFabric = new  FabricTest();
                $modelFabric->scenario ='create_nomenklatura';
                $modelFabric->project_id = $model->project_id;
                $modelFabric->nomenclatura_id = $model->id;
                $modelFabric->type = 'create';
                $modelFabric->save();

            } else {
                return $this->render('create', [
                    'model' => $model,
                    'project_id'=>$project_id,
                    'status' => ArrayHelper::map(Status::find()->all(), 'id', 'name'),
                    'upload' => $upload,
                    'file' => $file,
                    'stage' => ArrayHelper::map(Stage::find()->where('id != 3 AND id != 4')->all(), 'id', 'name'),
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Nomenclatura model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $project_id = $model->project_id;


//        $request = Yii::$app->request;
//        $model = new Nomenclatura();
        $upload = new UploadForm();
        $file = new UploadForm();
//        $upload->scenario = 'create';
//        $file->scenario = 'documents';
//        if($request->isAjax){
//            /*
//            *   Process for ajax request
//            */
//            Yii::$app->response->format = Response::FORMAT_JSON;
//            if($request->isGet){
//                return [
//                    'title'=> "Добавить  Накладную",
//                    'content'=>$this->renderAjax('create', [
//                        'model' => $model,
//                        'project_id'=>$project_id,
//                        'status' => ArrayHelper::map(Status::find()->all(), 'id', 'name'),
//                        'upload' => $upload,
//                        'file' => $file,
//                        'stage' => ArrayHelper::map(Stage::find()->where('id != 3 AND id != 4')->all(), 'id', 'name'),
//                    ]),
//                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
//                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
//
//                ];
//            }else if($model->load($request->post()) ){
//                $model->datetime = strtotime($model->datetime);
//                $model->save();
//                $model = Nomenclatura::findOne( $model->id);
//                $model->name = 'Накладная №'.$model->id.' за ' .date('d.m.Y ', $model->datetime);
//                $model->save();
//
//                $modelFabric = new  FabricTest();
//                $modelFabric->scenario ='create_nomenklatura';
//                $modelFabric->project_id = $model->project_id;
//                $modelFabric->nomenclatura_id = $model->id;
//                $modelFabric->type = 'create';
//                $modelFabric->save();
//
//                $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
//                $upload->material($model->id, $model->project_id);
//                $file->files = UploadedFile::getInstances($file, 'files');
//                $file->materialDocuments($model->id, $model->project_id);
//
//                return [
//                    'forceReload'=>'#crud-datatable-pjax',
//                    'title'=> "Добавить  Накладную",
//                    'content'=>'<span class="text-success">Накладная добавлена</span>',
//                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
//                        Html::a('Добавить Еще',['create','project_id'=>$project_id],['class'=>'btn btn-primary','role'=>'modal-remote'])
//
//                ];
//            }else{
//                return [
//                    'title'=> "Добавить  Накладную",
//                    'content'=>$this->renderAjax('update', [
//                        'model' => $model,
//                        'project_id'=>$project_id,
//                        'status' => ArrayHelper::map(Status::find()->all(), 'id', 'name'),
//                        'upload' => $upload,
//                        'file' => $file,
//                        'stage' => ArrayHelper::map(Stage::find()->where('id != 3 AND id != 4')->all(), 'id', 'name'),
//                    ]),
//                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
//                        Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
//
//                ];
//            }
//        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) ) {

                $model->datetime = strtotime($model->datetime);
                $model->save();

                $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
                $upload->material($model->id, $model->project_id);
                $file->files = UploadedFile::getInstances($file, 'files');
                $file->materialDocuments($model->id, $model->project_id);
                return $this->redirect(['/fabric-test/index', 'id' => $model->project_id]);


            } else {
                return $this->render('update', [
                    'model' => $model,
                    'project_id'=>$project_id,
                    'status' => ArrayHelper::map(Status::find()->all(), 'id', 'name'),
                    'upload' => $upload,
                    'file' => $file,
                    'stage' => ArrayHelper::map(Stage::find()->where('id != 3 AND id != 4')->all(), 'id', 'name'),
                ]);
            }
//        }
    }

    /**
     * Delete an existing Nomenclatura model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Nomenclatura model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Nomenclatura model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Nomenclatura the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Nomenclatura::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
