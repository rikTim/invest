<?php

namespace app\controllers;

use app\models\AuthAssignment;
use Yii;
use app\models\Users;
use app\models\UsersSeach;
use yii\base\Response;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),

                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],

                    [
                        'actions' => ['index','create','view','update'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersSeach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,1);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Users();
        $auth = new AuthAssignment();
        $role = Yii::$app->params['userRole'];

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        else if ($model->load(Yii::$app->request->post()) && $auth->load(Yii::$app->request->post())) {

            $model->setAttributes([
                'password_hash' => Yii::$app->security->generatePasswordHash($model->password),
                'auth_key' => Yii::$app->security->generateRandomString(),
                'created_at' => time(),
                'updated_at' => time(),
                'username' => $model->email,
            ]);
            $model->save();
            $auth->setAttributes([
                'user_id' => $model->id,
                'created_at' => time(),
            ]);
            $auth->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'auth' => $auth,
                'role' => $role,
            ]);
        }

    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if($id== 1)
        {
            $this->redirect(['index']);
        }
        $model = $this->findModel($id);
        $auth =AuthAssignment::find()->where(['user_id'=>$id])->one();
        $role = Yii::$app->params['userRole'];
        if ($model->load(Yii::$app->request->post()) && $auth->load(Yii::$app->request->post())) {
            $model->setAttributes([
                'password_hash' => Yii::$app->security->generatePasswordHash($model->password),
                'auth_key' => Yii::$app->security->generateRandomString(),
                'created_at' => time(),
                'updated_at' => time(),
                'username' => $model->email,
            ]);
            $model->save(false);
            $auth->setAttributes([
                'user_id' => $model->id,
                'created_at' => time(),
            ]);
            $auth->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'auth' => $auth,
                'role' => $role,
            ]);
        }
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
//    public function actionDelete($id)
//    {
//        $this->findModel($id)->delete();
//
//        return $this->redirect(['index']);
//    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
