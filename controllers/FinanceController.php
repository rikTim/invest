<?php

namespace app\controllers;

use app\models\AuthAssignment;
use app\models\FabricSearch;
use app\models\FabricTestSearch;
use app\models\Investor;
use app\models\InvestorMoney;
use app\models\InvestorMoneySeach;
use app\models\InvestorProject;
use app\models\MaterialMoney;
use app\models\MaterialSearch;
use app\models\ProjectSearch;
use app\models\Stage;
use app\models\Status;
use app\models\UploadForm;
use app\models\Works;
use Yii;
use app\models\Work;
use app\models\WorkSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * WorkController implements the CRUD actions for Work model.
 */
class FinanceController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),

                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],

                    ],
                    [
                        'actions' => ['main','index','create-money','money','update','view','stage','material'],
                        'allow' => true,
                        'roles' => ['admin','investor'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Work models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $searchModel = new WorkSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'id' => $id,
        ]);
    }

    /**
     * Displays a single Work model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $project_id = $model->project_id;
        $model->datetime = date('d.m.Y H:i:s', $model->datetime);
//        $model->operator_id = $model->operator->name;
        $model->status_id = $model->status->name;
        $model->stage_id = $model->stage->name;
        $model->work_id = $model->work->name;
        $model->project_id = $model->project->name;
        return $this->render('view', [
            'model' => $model,
            'project_id' => $project_id,
        ]);
    }

    /**
     * Finds the Work model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Work the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Work::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionMain()
    {
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $sum = 0;
        $invest = 0;
        if(!Yii::$app->user->can('admin')) {
            $money = InvestorMoney::find()->where(['investor_id'=>Yii::$app->user->id])->all();
            foreach ($money as $value) {
                $sum += $value->money_usd;
            }
            $money = MaterialMoney::find()->where(['investor_id' => Yii::$app->user->id])->all();

            foreach ($money as $value) {
                $kurs= InvestorMoney::find()->where('datetime <'.$value->datetime)->one();
                $invest += round($value->money/$kurs->kurs,2);

            }
        }else{
            $money = InvestorMoney::find()->all();
            foreach ($money as $value) {
                $sum += $value->money;
            }
            $money = MaterialMoney::find()->all();

            foreach ($money as $value) {

                $invest += $value->money;
            }
        }

        return $this->render('main', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'sum' => $sum,
            'invest'=>$invest,
        ]);
    }

    public function actionWork()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {

            $parents = $_POST['depdrop_parents'];

            if ($parents != null) {
                $cat_id = $parents[0];

                $out = self::getWorkList($cat_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function getWorkList($work_id)
    { // could be a static func as well
        $data = Works::find()->where(['state_id' => $work_id])->all();
        $value = (count($data) == 0) ? ['' => ''] : $data;

        return $value;
    }

    public function actionMoney()
    {
        $searchModel = new InvestorMoneySeach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('money', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    }

    public function actionCreateMoney()
    {
        $model = new InvestorMoney();
        $user = [];
        if (Yii::$app->user->can('admin')) {
            $investors = AuthAssignment::find()->where(['item_name' => 'investor'])->all();
            foreach ($investors as $investor) {
                if ($investor->user->status == 10) {
                    $user [$investor->user_id] = $investor->user->name;
                }
            }
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['money']);
        } else {
            return $this->render('create-money', [
                'model' => $model,
                'status' => ArrayHelper::map(Status::find()->all(), 'id', 'name'),
                'user' => $user,
            ]);
        }
    }


    public function actionMaterial($id)
    {
        $searchModel = new FabricTestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id);

        return $this->render('material', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'project_id'=>$id,
        ]);
    }


}
