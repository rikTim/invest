<?php

namespace app\controllers;

use app\models\AuthAssignment;
use app\models\Brigade;
use app\models\File;
use app\models\Investor;
use app\models\Material;
use app\models\MaterialMoney;
use app\models\Project;
use app\models\ProjectSearch;
use app\models\Stage;
use app\models\Status;
use app\models\UploadForm;
use app\models\Users;
use app\models\WorkMoney;
use app\models\Works;
use Yii;
use app\models\Work;
use app\models\WorkSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * WorkController implements the CRUD actions for Work model.
 */
class WorkController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),

                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['main', 'index', 'create', 'update', 'view', 'stage', 'work', 'success'],
                        'allow' => true,
                        'roles' => ['admin', 'moder', 'operatorWork', 'operatorMaterial'],
                    ],

                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['investor'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Work models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $model = Project::findOne($id);
        $searchModel = new WorkSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Work model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $files = File::find()->where(['work_id' => $id])->all();

        $work_id = $model->work_id;
        $project_id = $model->project_id;
        $model->datetime = date('d.m.Y H:i:s', $model->datetime);
        $model->brigade_id = $model->brigade->name;
        $model->status_id = $model->status->name;
        $model->stage_id = $model->stage->name;
        $model->work_id = $model->work->name;
        $model->project_id = $model->project->name;

        return $this->render('view', [
            'model' => $model,
            'work_id' => $work_id,
            'project_id' => $project_id,
            'files' => $files,
        ]);
    }

    /**
     * Creates a new Work model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($project_id)
    {
        $model = new Work();
        $upload = new UploadForm();
//        $money = new WorkMoney();
        $file = new UploadForm();
//        $upload->scenario = 'create';
//        $file->scenario = 'documents';
//        $project = Project::findOne($project_id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

//            $money->work_id = $model->id;
//            $money->project_id = $model->project_id;
//            $money->kurs = $model->kurs;
//            $money->save();
            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            $upload->money($model->id, $project_id);

            $file->files = UploadedFile::getInstances($file, 'files');
            $file->workDocuments($model->id, $project_id);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'project_id' => $project_id,
                'status' => ArrayHelper::map(Status::find()->all(), 'id', 'name'),
                'stage' => ArrayHelper::map(Stage::find()->all(), 'id', 'name'),
                'brigade' => ArrayHelper::map(Brigade::find()->all(), 'id', 'name'),
                'upload' => $upload,
                'file' => $file,
            ]);
        }
    }

    /**
     * Updates an existing Work model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $upload = new UploadForm();
//        $money = new WorkMoney();
        $file = new UploadForm();
        if ($model->status_id == 1) {


            if ($model->load(Yii::$app->request->post())) {
                $model->money = $model->price * $model->number;
                $model->save();

//                $money->work_id = $model->id;
//                $money->project_id = $model->project_id;
//                $money->kurs = $model->kurs;
//                $money->save();
                $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
                $upload->money($model->id, $model->project_id);

                $file->files = UploadedFile::getInstances($file, 'files');
                $file->workDocuments($model->id, $model->project_id);
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'status' => ArrayHelper::map(Status::find()->all(), 'id', 'name'),
                    'stage' => ArrayHelper::map(Stage::find()->all(), 'id', 'name'),
                    'brigade' => ArrayHelper::map(Brigade::find()->all(), 'id', 'name'),
                    'upload' => $upload,
//                    'money' => $money,
                    'file' => $file,
                ]);
            }
        }
        return $this->redirect(['index', 'id' => $model->project_id]);
    }


    /**
     * Finds the Work model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Work the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Work::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionMain()
    {
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('main', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionWork()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {

            $parents = $_POST['depdrop_parents'];

            if ($parents != null) {
                $cat_id = $parents[0];

                $out = self::getWorkList($cat_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function getWorkList($work_id)
    { // could be a static func as well
        $data = Works::find()->where(['state_id' => $work_id])->all();
        $value = (count($data) == 0) ? ['' => ''] : $data;

        return $value;
    }

    public function actionStage()
    {

        $model = new Works();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            return $this->redirect(['index']);
        } else {
            return $this->render('stage', [
                'model' => $model,
                'stage' => ArrayHelper::map(Stage::find()->all(), 'id', 'name'),

            ]);
        }


    }


    public function actionSuccess($id)
    {
        $request = Yii::$app->request;
        $workModel = Work::findOne($id);
        $workModel->datetime = date('d.m.Y H:i:s', $workModel->datetime);
        $workModel->brigade_id = $workModel->brigade->name;
        $workModel->status_id =$workModel->status->name;
        $workModel->stage_id = $workModel->stage->name;
        $workModel->work_id = $workModel->work->name;
        $workModel->project_id = $workModel->project->name;
        $work_moneys = WorkMoney::find()->where(['work_id' => $id])->all();
        $money = 0;
        foreach ($work_moneys as $work_money) {
            $money += $work_money->money;
        }
        $investors = AuthAssignment::find()->where(['item_name' => 'investor'])->all();
        $user = [];
        foreach ($investors as $investor) {
            if ($investor->user->status == 10) {
                $sum = Investor::sumMoney($investor->user_id);
                $user [$investor->user_id] = $investor->user->name.'('.$sum.')';
            }
        }
//        print_r($workModel->money);
//        print_r($money);die();
        $model = new MaterialMoney();
//        $model->scenario = 'update';
        if ($model->load($request->post()) ) {
            $model->save();
            $workModel = Work::findOne($id);
            $workModel->status_id = 2;
            $workModel->save();
            return $this->redirect(['index', 'id' => $workModel->project_id]);
        } else {
            return $this->render('success', [
                'model' => $model,
                'id' => $id,
                'workModel' => $workModel,
                'money' => $money,
                'user'=>$user,
            ]);
        }


    }

}
