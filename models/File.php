<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "file".
 *
 * @property integer $id
 * @property integer $path
 * @property integer $project_id
 * @property integer $work_id
 * @property integer $fabric_id
 */
class File extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['path','type'], 'required'],
            [['path', 'project_id', 'work_id', 'fabric_id','type'], 'integer'],
            [['path'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'path' => 'Path',
            'project_id' => 'Project ID',
            'work_id' => 'Work ID',
            'fabric_id' => 'Fabric ID',
        ];
    }
}
