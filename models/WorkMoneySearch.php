<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\WorkMoney;

/**
 * WorkMoneySearch represents the model behind the search form about `app\models\WorkMoney`.
 */
class WorkMoneySearch extends WorkMoney
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'work_id'], 'integer'],
            [['money', 'text', 'datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$id)
    {
        $query = WorkMoney::find()->where(['work_id'=>$id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'work_id' => $this->work_id,
        ]);

        $query->andFilterWhere(['like', 'money', $this->money])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'datetime', $this->datetime]);

        return $dataProvider;
    }
}
