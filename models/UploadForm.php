<?php
/**
 * Created by PhpStorm.
 * User: kolia
 * Date: 10.08.16
 * Time: 14:32
 */

namespace app\models;

use Imagine\Image\ManipulatorInterface;
use yii\base\Model;
use yii\helpers\BaseFileHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;
use yii\imagine\Image;

class UploadForm extends Model
{

    public $imageFile;
    public $files;

    public function rules()
    {
        return [
            [['imageFile'], 'required','on'=>'project'],

            [['imageFile'], 'image', 'minHeight' => 100, 'minWidth' => 100, 'extensions' => 'png, jpg, jpeg'],

            [['files'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png,jpg,jpeg', 'maxFiles' => 4,'on'=>'images'],
//            [['files'], 'required','on'=>'workersPhoto'],
            [['files'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png,jpg,jpeg', 'maxFiles' => 4,'on'=>'workersPhoto'],


            [['files'], 'file', 'skipOnEmpty' => false,  'maxFiles' => 10,'on'=>'documents'],
        ];
    }


    /**
     Photos -> type:
     * 1 = project_avatar(one)
     * 2 = money_picture(one)
     * 3 = stage_picture(many)
     * 4 = workers_picture(many)
     * 5 = material(money_picture)(one)
     * 6 = material_photo(one)
     */


    /**
    file -> type:
     * 1 = project document(many)
     * 2 = money document(many)
     * 3 = material document(many)
     */


    /**
     * @param $dir
     * @param $imgName
     * @throws \yii\base\Exception
     */
    protected function autoResize($dir, $imgName)
    {
        $dirCropped = $dir . '/cropped/';
        BaseFileHelper::createDirectory($dirCropped);
        Image::thumbnail($dir . '/' . $imgName, 200, 200, ManipulatorInterface::THUMBNAIL_INSET)
            ->save($dirCropped . $imgName);

    }

    /**
     * @param $id  =  project_id
     * @return bool|int
     * @throws \yii\base\Exception
     * Загрузка аватарки Project
     */
    public function avatar($id)
    {

        if ($this->imageFile && $this->validate()) {
            $dir = 'images/project/' . $id;
            $imgName = md5(time() . $this->imageFile->baseName) . '.' . $this->imageFile->extension;

            BaseFileHelper::createDirectory($dir);
            $this->imageFile->saveAs($dir . '/' . $imgName);

            $this->autoResize($dir, $imgName);

            $photoModel = new Photo();
            $photoModel->setAttributes([
                'path' => $imgName,
                'project_id' => $id,
                'type' => 1,
            ]);
            $photoModel->save();

            $model = new Project();
            $model->updateAll(['avatar_id' => $photoModel->id], 'id  =' . $id);
            return $photoModel->id;
        } else {
            return false;
        }
    }



    public function materialAvatar($id)
    {

        if ($this->imageFile && $this->validate()) {
            $dir = 'images/materialAvatar/' . $id;
            $imgName = md5(time() . $this->imageFile->baseName) . '.' . $this->imageFile->extension;

            BaseFileHelper::createDirectory($dir);
            $this->imageFile->saveAs($dir . '/' . $imgName);

            $this->autoResize($dir, $imgName);

            $photoModel = new Photo();
            $photoModel->setAttributes([
                'path' => $imgName,
                'material_id' => $id,
                'type' => 6,
            ]);
            $photoModel->save();

            $model = new Material();
            $model->updateAll(['photo_id' => $photoModel->id], 'id  =' . $id);
            return $photoModel->id;
        } else {
            return false;
        }
    }

    /**
     * @param $id = work_id
     * @param $project_id
     * @return bool|int
     * @throws \yii\base\Exception
     * Загрузка фотки к выполненым работам
     */
    public function money($id,$project_id)
    {


        if ($this->imageFile && $this->validate()) {

            $dir = 'images/work/' . $id;
            $imgName = md5(time() . $this->imageFile->baseName) . '.' . $this->imageFile->extension;

            BaseFileHelper::createDirectory($dir);
            $this->imageFile->saveAs($dir . '/' . $imgName);

            $this->autoResize($dir, $imgName);

            $photoModel = new Photo();
            $photoModel->setAttributes([
                'path' => $imgName,
                'work_id' => $id,
                'project_id' => $project_id,
                'type' => 2,
            ]);
            $photoModel->save();

            $model = new Work();
            $model->updateAll(['money_picture' => $photoModel->id], 'id  =' . $id);
            return $photoModel->id;
        } else {
            return false;
        }
    }

    /**
     * @param $id = fabric_id
     * @param $project_id
     * @return bool|int
     * @throws \yii\base\Exception
     * Загрузка фоток к приобретенным материалам
     */
    public function material($id,$project_id)
    {


        if ($this->imageFile && $this->validate()) {

            $dir = 'images/nomenclatura/' . $id;
            $imgName = md5(time() . $this->imageFile->baseName) . '.' . $this->imageFile->extension;

            BaseFileHelper::createDirectory($dir);
            $this->imageFile->saveAs($dir . '/' . $imgName);

            $this->autoResize($dir, $imgName);

            $photoModel = new Photo();
            $photoModel->setAttributes([
                'path' => $imgName,
                'nomenclatura_id' => $id,
                'project_id' => $project_id,
                'type' => 5,
            ]);
            $photoModel->save();

            $model = new Nomenclatura();
            $model->updateAll(['avatar_id' => $photoModel->id], 'id  =' . $id);
            return $photoModel->id;
        } else {
            return false;
        }
    }

    /**
     * @param $id = stage_id
     * @param $project_id
     * @return bool
     * @throws \yii\base\Exception
     *
     * Загрузка фоток стадии Project
     */
    public function photos($id,$project_id)
    {
        $this->scenario = 'images';
        if ($this->validate()) {
            foreach ($this->files as $file) {
                $dir = 'images/stage/' . $id;
                $imgName = md5(time() . $file->baseName) . '.' . $file->extension;

                BaseFileHelper::createDirectory($dir);
                $file->saveAs($dir . '/' . $imgName);

                $this->autoResize($dir, $imgName);

                $photoModel = new Photo();
                $photoModel->setAttributes([
                    'path' => $imgName,
                    'stage_id' => $id,
                    'project_id'=>$project_id,
                    'type' => 3,
                ]);
                $photoModel->save();
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $id = workers_id
     * @return bool
     * @throws \yii\base\Exception
     * Загрузка Workers фоток
     */
    public function workers($id)
    {

        if ($this->validate()) {
            foreach ($this->files as $file) {
                $dir = 'images/workers/' . $id;
                $imgName = md5(time() . $file->baseName) . '.' . $file->extension;

                BaseFileHelper::createDirectory($dir);
                $file->saveAs($dir . '/' . $imgName);

                $this->autoResize($dir, $imgName);

                $photoModel = new Photo();
                $photoModel->setAttributes([
                    'path' => $imgName,
                    'workers_id' => $id,
                    'type' => 4,
                ]);
                $photoModel->save();

                $model = new Workers();
                $model->updateAll(['photo_id' => $photoModel->id], 'id  =' . $id);
            }
            return true;
        } else {
            return false;
        }
    }


    public function workDocuments($id,$project_id)
    {

        if ($this->validate()) {
            foreach ($this->files as $file) {
                $dir = 'images/file/' . $id;
                $imgName = md5(time() . $file->baseName) . '.' . $file->extension;

                BaseFileHelper::createDirectory($dir);
                $file->saveAs($dir . '/' . $imgName);



                $fileModel = new File();
                $fileModel->setAttributes([
                    'path' => $imgName,
                    'work_id' => $id,
                    'project_id'=>$project_id,
                    'type' => 2,
                ]);
                $fileModel->save(false);

            }
            return true;
        } else {
            return false;
        }
    }


    public function materialDocuments($id,$project_id)
    {

        if ($this->validate()) {
            foreach ($this->files as $file) {
                $dir = 'images/file/' . $id;
                $imgName = md5(time() . $file->baseName) . '.' . $file->extension;

                BaseFileHelper::createDirectory($dir);
                $file->saveAs($dir . '/' . $imgName);



                $fileModel = new File();
                $fileModel->setAttributes([
                    'path' => $imgName,
                    'nomenclatura_id' => $id,
                    'project_id'=>$project_id,
                    'type' => 3,
                ]);
                $fileModel->save(false);

            }
            return true;
        } else {
            return false;
        }
    }


    public function projectDocuments($id)
    {

        if ($this->validate()) {

            foreach ($this->files as $file) {
                $dir = 'images/file/' . $id;
                $imgName = md5(time() . $file->baseName) . '.' . $file->extension;

                BaseFileHelper::createDirectory($dir);
                $file->saveAs($dir . '/' . $imgName);



                $fileModel = new File();
                $fileModel->setAttributes([
                    'path' => $imgName,
                    'project_id'=>$id,
                    'type' => 1,
                ]);
                $fileModel->save(false);

            }
            return true;
        } else {
            return false;
        }
    }

}