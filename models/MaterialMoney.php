<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "material_money".
 *
 * @property integer $id
 * @property integer $material_id
 * @property string $datetime
 * @property string $money
 * @property string $text
 */
class MaterialMoney extends \yii\db\ActiveRecord
{
    public $material_name;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material_money';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['nomenclatura_id','investor_id'], 'required'],
            [['nomenclatura_id','project_id','investor_id','type'], 'integer'],
            [['text'], 'string'],
            [['datetime'], 'string', 'max' => 255],
            [['money'], 'number'],
//            [['kurs'],'required','on'=>'update']
//            ['money','validateMoney','skipOnEmpty'=>false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nomenclatura_id' => 'Номенклатура',
            'datetime' => 'Дата',
            'money' => 'Оплата',
            'text' => 'Примечание',
            'investor_id'=> 'Инвестор',
            'type'=>'Тип оплаты',
        ];
    }
    public function getNomenclatura()
    {
        return $this->hasOne(Nomenclatura::className(), ['id' => 'nomenclatura_id']);
    }

    public function getWork()
    {
        return $this->hasOne(Work::className(), ['id' => 'work_id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->datetime = time();
                $this->type = 2;
            }
            return true;
        }
        return false;
    }

//    public function validateMoney()
//    {
//        $model = Fabric::find()->where(['id'=>$this->nomenclatura_id])->one();
//        $material_money = MaterialMoney::find()->where(['nomenclatura_id'=>$this->nomenclatura_id])->all();
//        $success = '';
//        foreach($material_money as $money){
//            $success += $money->money;
//        }$minus = 0;
//        if(!empty($model)) {
//            $minus = $model->money;
//        }$sum = $minus - $success;
//        if($this->money > $sum) {
//            $this->addError('money', ' Оплата не больше ' . $sum . '');
//        }
//    }
}
