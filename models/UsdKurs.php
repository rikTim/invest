<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usd_kurs".
 *
 * @property integer $id
 * @property string $kurs
 * @property string $datetime
 */
class UsdKurs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usd_kurs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kurs', 'datetime'], 'required'],
            [['kurs'], 'string', 'max' => 45],
            [['datetime'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kurs' => 'Kurs',
            'datetime' => 'Datetime',
        ];
    }
}
