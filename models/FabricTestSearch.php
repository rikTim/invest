<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FabricTest;

/**
 * FabricTestSearch represents the model behind the search form about `app\models\FabricTest`.
 */
class FabricTestSearch extends FabricTest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'material_id', 'money_picture', 'project_id', 'is_paid', 'nomenclatura_id'], 'integer'],
            [['money', 'price', 'number'], 'number'],
            [['kurs'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$id)
    {
        $query = FabricTest::find();
        $query->addOrderBy(['nomenclatura_id'=> SORT_DESC]);
        if(Yii::$app->user->can('investor')){
            $query->joinWith('nomenclatura');
            $query->andWhere(['status_id'=>2]);
        }
        $query->andWhere(['fabric.project_id'=>$id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'material_id' => $this->material_id,
            'money' => $this->money,
            'money_picture' => $this->money_picture,
            'project_id' => $this->project_id,
            'is_paid' => $this->is_paid,
            'price' => $this->price,
            'number' => $this->number,
            'nomenclatura_id' => $this->nomenclatura_id,
        ]);

        $query->andFilterWhere(['like', 'kurs', $this->kurs]);

        return $dataProvider;
    }
}
