<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Fabric;

/**
 * FabricSearch represents the model behind the search form about `app\models\Fabric`.
 */
class FabricSearch extends Fabric
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id',  'status_id', 'material_id', 'money_picture', 'project_id'], 'integer'],
            [['datetime','brigade', 'money', 'kurs'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$id)
    {
        $query = Fabric::find()->where(['project_id'=>$id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status_id' => $this->status_id,
            'material_id' => $this->material_id,
            'money_picture' => $this->money_picture,
            'project_id' => $this->project_id,
        ]);

        $query->andFilterWhere(['like', 'datetime', $this->datetime])
            ->andFilterWhere(['like', 'brigade', $this->brigade])
            ->andFilterWhere(['like', 'money', $this->money])
            ->andFilterWhere(['like', 'kurs', $this->kurs]);

        return $dataProvider;
    }
}
