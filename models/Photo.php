<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "photo".
 *
 * @property integer $id
 * @property integer $type
 * @property string $path
 * @property integer $project_id
 * @property integer $stage_id
 * @property integer $work_id
 */
class Photo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'photo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'path'], 'required'],
            [['type', 'project_id', 'stage_id', 'work_id','workers_id','nomenclatura_id'], 'integer'],
            [['path'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'path' => 'Path',
            'project_id' => 'Project ID',
            'stage_id' => 'Stage ID',
            'work_id' => 'Work ID',
        ];
    }
}
