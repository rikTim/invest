<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MaterialMoney;

/**
 * MaterialMoneySearch represents the model behind the search form about `app\models\MaterialMoney`.
 */
class MaterialMoneySearch extends MaterialMoney
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nomenclatura_id'], 'integer'],
            [['datetime', 'money', 'text'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$id)
    {
        $query = MaterialMoney::find()->where(['nomenclatura_id'=>$id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'nomenclatura_id' => $this->nomenclatura_id,
        ]);

        $query->andFilterWhere(['like', 'datetime', $this->datetime])
            ->andFilterWhere(['like', 'money', $this->money])
            ->andFilterWhere(['like', 'text', $this->text]);

        return $dataProvider;
    }

    public function searchMoney($params,$id)
    {
        if(!empty($id)) {
            $query = MaterialMoney::find()->where(['investor_id' => $id])->orderBy(['datetime' => SORT_DESC]);
        }else{
            $query = MaterialMoney::find()->orderBy(['datetime' => SORT_DESC]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'nomenclatura_id' => $this->nomenclatura_id,
        ]);

        $query->andFilterWhere(['like', 'datetime', $this->datetime])
            ->andFilterWhere(['like', 'money', $this->money])
            ->andFilterWhere(['like', 'text', $this->text]);

        return $dataProvider;
    }

}
