<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "material".
 *
 * @property integer $id
 * @property string $name
 * @property string $number
 * @property integer $photo_id
 * @property string $units
 * @property string $per_unit
 */
class Material extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'number', 'units', 'per_unit'], 'required'],
            [['photo_id'], 'integer'],
            [['name', 'number', 'per_unit'], 'string', 'max' => 255],
            [['units'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'number' => 'Номер',
            'photo_id' => 'Фото',
            'units' => 'Единица  Измерения',
            'per_unit' => 'Сумма за 1 единицу',
        ];
    }


    public function getPhoto()
    {
        return $this->hasOne(Photo::className(), ['id' => 'photo_id']);
    }

}
