<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nomenclatura".
 *
 * @property integer $id
 * @property string $provider
 * @property integer $project_id
 * @property integer $status_id
 * @property string $datetime
 */
class Nomenclatura extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nomenclatura';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provider', 'project_id', 'status_id', 'datetime','stage_id'], 'required'],
            [['project_id', 'status_id','stage_id'], 'integer'],
            [['provider','name','avatar_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'provider' => 'Поставщик',
            'project_id' => 'Project ID',
            'status_id' => 'Статус',
            'datetime' => 'Datetime',
            'stage_id'=>'Этап',
        ];
    }

//    public function afterSave($insert)
//    {
//        $model = Nomenclatura::findOne($this->id);
//        $model->name = 'Номенклатура №'.$model->id.' за ' .date('d.m.Y ', $model->datetime);
//        $model->save();
//    }

    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    public function getStage()
    {
        return $this->hasOne(Stage::className(), ['id' => 'stage_id']);
    }

    public function getAvatar()
    {
        return $this->hasOne(Photo::className(), ['id' => 'avatar_id']);
    }


    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    public function getSum()
    {
        return $this->hasMany(MaterialMoney::className(), ['nomenclatura_id' => 'id'])->sum('money');
    }

    public function getMaterial(){
        return $this->hasMany(FabricTest::className(), ['nomenclatura_id'=>'id'])->sum('money');
    }

    public function getMaterialName()
    {
        return $this->hasMany(FabricTest::className(), ['nomenclatura_id'=>'id']);
    }


    public static function getMaterialNames($id)
    {
        $materialNames = FabricTest::find()->where(['nomenclatura_id'=>$id])->all();

        $material_name='';
        foreach($materialNames as $materialName){
            $material_name .= '<a href="'.\yii\helpers\Url::to(['fabric-test/view','id'=>$materialName->id]).'">'.$materialName->material->name.'</a>&nbsp;&nbsp;/&nbsp;&nbsp;';
        }

        return  $material_name;
    }

}
