<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sender_sql".
 *
 * @property integer $id
 * @property string $timestamp
 * @property string $datetime
 * @property string $send_message
 */
class SenderSql extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sender_sql';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['timestamp', 'datetime'], 'required'],
            [['timestamp', 'datetime', 'send_message'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'timestamp' => 'Timestamp',
            'datetime' => 'Datetime',
            'send_message' => 'Send Message',
        ];
    }
}
