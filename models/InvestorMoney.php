<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "investor_money".
 *
 * @property integer $id
 * @property string $money
 * @property string $datetime
 * @property string $kurs
 * @property integer $project_id
 * @property integer $investor_id
 */
class InvestorMoney extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'investor_money';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['money_usd',  'kurs', 'investor_id'], 'required'],
            [['investor_id','money','money_usd'], 'integer'],
            [[ 'datetime', 'kurs'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'money' => 'Деньги',
            'money_usd' => 'Деньги',
            'datetime' => 'Время',
            'kurs' => 'Курс',
//            'project_id' => 'Проект',
            'investor_id' => 'Инвестор',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->datetime = time();
                $this->money = $this->money_usd *$this->kurs;
            }
            return true;
        }
        return false;
    }

//    public  function getProject()
//    {
//        return $this->hasOne(Project::className(), ['id' => 'project_id']);
//    }

    public  function getInvestor()
    {
        return $this->hasOne(Users::className(), ['id' => 'investor_id']);
    }

}
