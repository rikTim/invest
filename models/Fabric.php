<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fabric".
 *
 * @property integer $id
 * @property string $datetime
 * @property integer $brigade_id
 * @property integer $status_id
 * @property integer $material_id
 * @property string $money
 * @property integer $money_picture
 * @property string $kurs
 * @property integer $project_id
 */
class Fabric extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fabric';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brigade', 'status_id', 'material_id','project_id', 'price', 'number'], 'required'],
            [['status_id', 'material_id', 'money_picture', 'project_id', 'is_paid'], 'integer'],
            [['money', 'price', 'number'], 'number'],
            [['datetime', 'brigade', 'kurs'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'datetime' => 'Дата',
            'brigade' => 'Поставщик',
            'status_id' => 'Статус',
            'material_id' => 'Материал',
            'money_picture' => 'Фото',
            'kurs' => 'Курс',
            'project_id' => 'Проект',
            'price'=>'Цена за единицу',
            'number'=>'Количество',
            'money' => 'Общая стоимость',
        ];
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->datetime = time();
                $this->money =  $this->price * $this->number;
            }
            return true;
        }
        return false;
    }

    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }


    public function getMaterial()
    {
        return $this->hasOne(Material::className(), ['id' => 'material_id']);
    }


    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }
    public function getSuccess()
    {
        return $this->hasMany(MaterialMoney::className(), ['material_id' => 'id'])->sum('money');

    }

    public function getAvatar()
    {
        return $this->hasOne(Photo::className(), ['id' => 'money_picture']);
    }


}
