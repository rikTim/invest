<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Investor;

/**
 * InvestorSearch represents the model behind the search form about `app\models\Investor`.
 */
class InvestorSearch extends Investor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'investor_id', 'project_id'], 'integer'],
            [['persent'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$id)
    {
        $query = Investor::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'investor_id' => $this->investor_id,
            'project_id' => $id,
        ]);

        $query->andFilterWhere(['like', 'persent', $this->persent]);

        return $dataProvider;
    }
}
