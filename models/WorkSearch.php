<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Work;

/**
 * WorkSearch represents the model behind the search form about `app\models\Work`.
 */
class WorkSearch extends Work
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'brigade_id', 'status_id', 'stage_id', 'work_id', 'project_id'], 'integer'],
            [['datetime', 'note', 'money', 'money_picture'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$id)
    {

        if(Yii::$app->user->can('admin') || Yii::$app->user->can('operatorWork')) {
            $query = Work::find();
        }
        if(Yii::$app->user->can('investor')){
            $query = Work::find()->where(['status_id' => 2]);
        }
        if(Yii::$app->user->can('moder')){
            $query = Work::find()->where(['operator_id'=>Yii::$app->user->id,'status_id' => 1]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'brigade_id' => $this->brigade_id,
            'status_id' => $this->status_id,
            'stage_id' => $this->stage_id,
            'work_id' => $this->work_id,
            'project_id' => $id,
        ]);

        $query->andFilterWhere(['like', 'datetime', $this->datetime])
            ->andFilterWhere(['like', 'note', $this->note])
            ->andFilterWhere(['like', 'money', $this->money])
            ->andFilterWhere(['like', 'money_picture', $this->money_picture]);

        return $dataProvider;
    }
}
