<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project".
 *
 * @property integer $id
 * @property string $name
 * @property string $kurs
 * @property string $datetime
 * @property integer $status
 */
class Project extends \yii\db\ActiveRecord
{

    public $investors;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'status'], 'required'],
            [['status', 'avatar_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['kurs', 'datetime'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'kurs' => 'Курс',
            'datetime' => 'Дата',
            'status' => 'Статус',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->datetime = time();
            }
            return true;
        }
        return false;
    }

    public function getAvatar()
    {
        return $this->hasOne(Photo::className(), ['id' => 'avatar_id']);
    }

    public function getStatuses()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }

    public function getSum($id)
    {
        $money = $this->getInvestion($id);
        $minus = $this->getMinus($id);
        print_r($minus);die();
        if (Yii::$app->user->can('admin')) {
            $sum = $money - $minus;
        } else {

            $investor = Investor::find()->where(['project_id' => $id, 'investor_id' => Yii::$app->user->id])->one();
            $persent = $investor->persent / 100;
            $minus = $minus * $persent;
            $sum = $money - $minus;
        }

        if ($sum < 0) {

            $return = '<span class="label bg-red "> ' . '-' . $sum . '</span>';
        }
        if ($sum == 0) {

            $return = '<span class="label bg-green "> ' . $sum . '</span>';
        }
        if ($sum > 0) {

            $return = '<span class="label bg-blue "> ' . '+' . $sum . '</span>';
        }


        return $return;
    }


    public function getSumAll($id)
    {

        $investor_money = InvestorProject::find()->where(['project_id' => $id])->all();
        $money = 0;
        foreach ($investor_money as $investor) {
            $money += $investor->money;
        }
        $material = $this->getMoneyMaterialMinus($id);
        $work = $this->getMoneyWorkMinus($id);
        $sum = $money - $material - $work;
        return $sum;
    }


    public function getSumAllusd($id)
    {

        $investor_money = InvestorProject::find()->where(['project_id' => $id])->all();
        $money = 0;
        foreach ($investor_money as $investor) {
            $money += $investor->money_usd;
        }
        $material = $this->getMoneyMaterialMinus($id);
        $work = $this->getMoneyWorkMinus($id);
        $sum = $money - $material - $work;
        return $sum;
    }


    public function getMoneyMaterialMinus($id)
    {
//        print_r($id);
        $materialMoney = MaterialMoney::find()->where(['project_id' => $id,'type'=>'2'])->all();
//        print_r($materialMoney);
        $minus = 0;
        foreach ($materialMoney as $money) {
            $minus += $money->money;
        }
        return $minus;
    }


    public function getMoneyWorkMinus($id)
    {
        $workMoney = WorkMoney::find()->where(['project_id' => $id,'type'=>'1'])->all();

        $minus = 0;
        foreach ($workMoney as $money) {
            $minus += $money->money;
        }
        return $minus;
    }

    public function getMoneyMaterialNoPaid($id)
    {
        $minus = $this->getMoneyMaterialMinus($id);
        $material = Fabric::find()->where(['project_id' => $id])->all();
//        print_r($material);die();
        $paid = 0;
        foreach ($material as $money) {
            $paid += $money->money;
        }
//        if(Yii::$app->user->can('admin')){
            $return = $paid - $minus;
//        }else {
//            $kurs = UsdKurs::find()->where(['datetime'=>date('Y-m-d')])->one();
//            $return = round(($paid - $minus)/$kurs->kurs,2);
//        }
        return $return;
    }

    public function getMoneyWorkNoPaid($id)
    {
        $minus = $this->getMoneyWorkMinus($id);
        $work = Work::find()->where(['project_id' => $id])->all();
        $paid = 0;
        foreach ($work as $money) {
            $paid += $money->money;
        }
//        if(Yii::$app->user->can('admin')){
            $return = $paid - $minus;
//        }else {
//            $kurs = UsdKurs::find()->where(['datetime'=>date('Y-m-d')])->one();
//            $return = round(($paid - $minus)/$kurs->kurs,2);
//        }
        return $return;
    }


    public function getInvestion($id)
    {
        if (Yii::$app->user->can('admin')) {
            $investor_money = InvestorProject::find()->where(['project_id' => $id])->all();
        } else {
            $investor_money = InvestorProject::find()->where(['project_id' => $id, 'investor_id' => Yii::$app->user->id])->all();
        }
        $money = 0;
        foreach ($investor_money as $investor) {
            $money += $investor->money;
        }
        return $money;
    }

    public function getInvestionusd($id)
    {
        if (Yii::$app->user->can('admin')) {
            $investor_money = InvestorProject::find()->where(['project_id' => $id])->all();
        } else {
            $investor_money = InvestorProject::find()->where(['project_id' => $id, 'investor_id' => Yii::$app->user->id])->all();
        }
        $money = 0;
        foreach ($investor_money as $investor) {
            $money += $investor->money_usd;
        }
        return $money;
    }


    public function getMinus($id)
    {
        if (Yii::$app->user->can('admin')) {
            $works = Work::find()->where(['project_id' => $id, 'status_id' => 2])->all();
        } else {
            $works = Work::find()->where(['project_id' => $id, 'status_id' => 2])->all();
        }
        $sum = 0;
        if (!empty($works)) {
            foreach ($works as $work) {
                $sum += $work->money;
            }
        }
        return $sum;
    }

//    public function getInvestor()
//    {
//        return $this->hasMany(Investor::className(), ['project_id' => 'id']);
//    }


    public function getInvestor()
    {
        return $this->hasMany(InvestorProject::className(), ['project_id'=>'id']);
    }

    public function getPersent($id)
    {
        $investor = Investor::find()->where(['project_id' => $id])->all();
        $persent = 0;
        if (!empty($investor)) {

            foreach ($investor as $invest) {
                $persent += $invest->persent;
            }
        }
        switch ($persent) {
            case $persent < 100:
                $return = '<center><span class="label bg-yellow "> ' . $persent . '%</span></center>';;
                break;
            case $persent == 100:
                $return = '<center><span class="label bg-green ">' . $persent . '%</span></center>';
                break;
            case $persent > 100:
                $return = '<span class="label bg-red "> ' . $persent . '</span>';
                break;
        }
        return $return;

    }

}
