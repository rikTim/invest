<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $password_hash
 * @property string $auth_key
 * @property integer $confirmed_at
 * @property string $unconfirmed_email
 * @property integer $blocked_at
 * @property string $registration_ip
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $flags
 * @property integer $status
 * @property string $name
 *
 * @property Profile $profile
 * @property SocialAccount[] $socialAccounts
 * @property Token[] $tokens
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'name','password'], 'required'],
            [['confirmed_at', 'blocked_at', 'created_at', 'updated_at', 'flags', 'status'], 'integer'],
            [['username', 'email', 'unconfirmed_email', 'name'], 'string', 'max' => 255],
            [['password_hash','password'], 'string', 'max' => 60],
            [['auth_key'], 'string', 'max' => 32],
            [['registration_ip'], 'string', 'max' => 45],
            [['email'], 'unique'],
            [['username'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
//            'username' => 'Никнейм',
            'email' => 'Email',
            'password_hash' => 'Password Hash',
            'auth_key' => 'Auth Key',
            'confirmed_at' => 'Confirmed At',
            'unconfirmed_email' => 'Unconfirmed Email',
            'blocked_at' => 'Blocked At',
            'registration_ip' => 'Registration Ip',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'flags' => 'Flags',
            'status' => 'Status',
            'name' => 'Имя',
            'password' => 'Пароль',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocialAccounts()
    {
        return $this->hasMany(SocialAccount::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTokens()
    {
        return $this->hasMany(Token::className(), ['user_id' => 'id']);
    }

    public function getRole($id)
    {
        $role = AuthAssignment::find()->where(['user_id'=>$id])->one();
        return $role->item_name;
    }

    public function getAuth()
    {
        return $this->hasOne(AuthAssignment::className(), ['user_id' => 'id']);
    }

    public  function getSum($user_id)
    {
        $investorMoney = InvestorMoney::find()->where(['investor_id'=>$user_id])->all();
        $sum = 0;
        foreach($investorMoney as $investor)
        {
            $sum += $investor->money;
        }

        return $sum;
    }


    public  function getMinus($user_id)
    {
        $investorMoney = InvestorProject::find()->where(['investor_id'=>$user_id])->all();
        $minus = 0;
        foreach($investorMoney as $investor)
        {
            $minus += $investor->money;
        }

        return $minus;
    }


    public  function getOther($user_id)
    {
        $sum = $this->getSum($user_id);
        $minus = $this->getMinus($user_id);

        return $sum-$minus;
    }

}
