<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "work_comment".
 *
 * @property integer $id
 * @property integer $work_id
 * @property string $text
 */
class WorkComment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'work_comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['work_id', 'text'], 'required'],
            [['work_id'], 'integer'],
            [['text'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'work_id' => 'Выполненые работы',
            'text' => 'Описание',
        ];
    }

    public function getWork()
    {
        return $this->hasOne(Work::className(), ['id' => 'work_id']);
    }

}
