<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "brigade".
 *
 * @property integer $id
 * @property string $name
 * @property string $create_datetime
 * @property string $finish_datetime
 * @property integer $status_id
 */
class Brigade extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brigade';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status_id'], 'integer'],
            [['name', 'create_datetime', 'finish_datetime'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'create_datetime' => 'Дата создания',
            'finish_datetime' => 'Дата окончания',
            'status_id' => 'Статус',
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->create_datetime = time();
                $this->status_id = 2;
            }
            return true;
        }
        return false;
    }


    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @throws \yii\db\Exception
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if($this->status_id == 3){

            Yii::$app->db->createCommand("UPDATE `team` SET `status_id`='3' ,`finish_datetime`=".time()." WHERE `brigade_id`='$this->id' ")->execute();

        }
    }
}
