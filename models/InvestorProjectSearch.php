<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\InvestorProject;

/**
 * InvestorProjectSearch represents the model behind the search form about `app\models\InvestorProject`.
 */
class InvestorProjectSearch extends InvestorProject
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'investor_id', 'project_id'], 'integer'],
            [['money', 'datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$id)
    {

        if(Yii::$app->user->can('admin')) {
            $query = InvestorProject::find()->where(['investor_id'=>$id]);
        }else{
            $query = InvestorProject::find()->where(['investor_id'=>Yii::$app->user->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'investor_id' => $this->investor_id,
            'project_id' => $this->project_id,
        ]);

        $query->andFilterWhere(['like', 'money', $this->money])
            ->andFilterWhere(['like', 'datetime', $this->datetime]);

        return $dataProvider;
    }
}
