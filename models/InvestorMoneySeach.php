<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\InvestorMoney;

/**
 * InvestorMoneySeach represents the model behind the search form about `app\models\InvestorMoney`.
 */
class InvestorMoneySeach extends InvestorMoney
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'investor_id'], 'integer'],
            [['money', 'datetime', 'kurs'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if(Yii::$app->user->can('admin')) {
            $query = InvestorMoney::find();
        }else{
            $query = InvestorMoney::find()->where(['investor_id'=>Yii::$app->user->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'investor_id' => $this->investor_id,
        ]);

        $query->andFilterWhere(['like', 'money', $this->money])
            ->andFilterWhere(['like', 'datetime', $this->datetime])
            ->andFilterWhere(['like', 'kurs', $this->kurs]);

        return $dataProvider;
    }
}
