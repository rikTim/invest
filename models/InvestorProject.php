<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "investor_project".
 *
 * @property integer $id
 * @property integer $investor_id
 * @property integer $project_id
 * @property string $money
 * @property string $datetime
 */
class InvestorProject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'investor_project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['investor_id', 'project_id',  'datetime','kurs'], 'required'],
            [['investor_id', 'project_id'], 'integer'],
            [['money', 'datetime','kurs','money_usd','money'], 'string', 'max' => 255],
//            [['money_usd'], 'validateCreate', 'skipOnEmpty' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'investor_id' => 'Investor ID',
            'project_id' => 'Проект',
            'money' => 'Деньги',
            'datetime' => 'Дата',
            'kurs'=>'Курс Долара',
            'money_usd'=>'Деньги $'
        ];
    }


    public function validateCreate()
    {
        $sum = InvestorMoney::find()->where(['investor_id'=>$this->investor_id])->all();
        $summa = 0;
        foreach($sum as $value){
            $summa += $value->money_usd;
        }

        $investors = InvestorProject::find()->where(['investor_id'=>$this->investor_id])->all();
        $minus = 0;
        foreach($investors as $investor)
        {
            $minus += $investor->money_usd;
        }
        $n = $summa - $minus;
        if ($this->money > $n) {
            $this->addError('money_usd', 'У инвестора недостаточно денег. Можно распределить не больше '.$n.'');
        }
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->money_usd = round($this->money/$this->kurs,2);
            }
            return true;
        }
        return false;
    }


    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    public  function getInvestor()
    {
        return $this->hasOne(Users::className(), ['id' => 'investor_id']);
    }


    public  function getSum($user_id)
    {
        $investorMoney = InvestorMoney::find()->where(['investor_id'=>$user_id])->all();
        $sum = 0;
        foreach($investorMoney as $investor)
        {
            $sum += $investor->money;
        }

        return $sum;
    }

}
