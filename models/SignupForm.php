<?php
namespace app\models;

use app\models\Users;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $name;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],

            ['username', 'unique', 'targetClass' => 'app\models\Users', 'message' => 'Такой никнейм уже используется'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email','required'],
            ['email', 'unique', 'targetClass' => 'app\models\Users', 'message' => 'Такой email уже используется'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            [['name'], 'string', 'max' => 45],


        ];
    }


    public function attributeLabels()
    {
        return [
            'name' => 'Имя ',

            'password' => 'Пароль',
            'email' => 'Email',

            'username' => 'Логин'
        ];
    }


    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        $user = new User();
        $user->username = $this->email;
        $user->email = $this->email;
        $user->name =  $this->name;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->save();

        return $user->save() ? $user : null;
    }
}
