<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "investor".
 *
 * @property integer $id
 * @property integer $investor_id
 * @property integer $project_id
 * @property string $persent
 */
class Investor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'investor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['investor_id', 'project_id', 'persent'], 'required'],
            [['investor_id', 'project_id'], 'integer'],
            [['persent'], 'integer'],


            [['persent'], 'validateCreate', 'skipOnEmpty' => false],


            ['investor_id','validateUser','skipOnEmpty'=>false,'on'=>'create']
//            [['persent'], 'validateUpdate', 'skipOnEmpty' => false,'on'=>'update'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'investor_id' => 'Инвестор',
            'project_id' => 'Проект',
            'persent' => 'Процент',
        ];
    }

    public  function getInvestor()
    {
        return $this->hasOne(Users::className(), ['id' => 'investor_id']);
    }

    public  function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }


    public function validateCreate()
    {
        $investors = Investor::find()->where(['project_id'=>$this->project_id])->all();
        $persent = 0;
        foreach($investors as $investor)
        {
            $persent += $investor->persent;
        }
        $n = 100 - $persent;
        if ($this->persent > $n) {
            $this->addError('persent', 'Общий процент не может быть больше 100%.Текущий процент не больше '.$n.'%');
        }
    }

//    public function validateUpdate()
//    {
//        $investors = Investor::find()->where(['project_id'=>$this->project_id])->all();
//        $persent = 0;
//        foreach($investors as $investor)
//        {
//            $persent += $investor->persent;
//        }
//        $investor = Investor::findOne($this->investor_id);
//        $persent = $persent- $investor->persent;
//        $n = 100 - $persent;
//        if ($this->persent > $n) {
//            $this->addError('persent', 'Общий процент не может быть больше 100%.Текущий процент не больше '.$n.'%');
//        }
//    }

    public function validateUser()
    {
        $investor = Investor::find()->where(['project_id'=>$this->project_id,'investor_id'=>$this->investor_id])->all();
        if(!empty($investor))
        {
         $this->addError('investor_id','Этот инвестор уже принимает участие в данном проекте');
        }
    }


    public static function investion($id)
    {
        $investion = InvestorMoney::find()->where(['investor_id'=>$id])->sum('money');
        if(empty($investion)){
            $investion = 0;
        }
        return $investion;
    }


    public static function materialMoney($id)
    {
        return $investion = MaterialMoney::find()->where(['investor_id'=>$id,'type'=>'2'])->sum('money');
    }


    public static function workMoney($id)
    {
        return $investion = WorkMoney::find()->where(['investor_id'=>$id,'type'=>'1'])->sum('money');
    }

    public static function sumMoney($id)
    {
        $investion = Investor::investion($id);
        $material = Investor::materialMoney($id);
        $work  = Investor::workMoney($id);
        return round($investion - $material - $work,2);
    }


}
