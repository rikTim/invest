<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "work".
 *
 * @property integer $id
 * @property string $datetime
 * @property integer $operator_id
 * @property integer $status_id
 * @property integer $stage_id
 * @property integer $work_id
 * @property string $note
 * @property string $money
 * @property string $money_picture
 * @property integer $project_id
 */
class Work extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'work';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brigade_id', 'status_id', 'stage_id', 'work_id',  'project_id', 'price', 'number'], 'required'],
            [['brigade_id', 'status_id', 'stage_id', 'work_id', 'money_picture', 'project_id'], 'integer'],
            [['note'], 'string'],
            [['money', 'price', 'number'], 'number'],
            [['datetime', 'kurs'], 'string', 'max' => 45],
        ];
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->datetime = time();
                $this->money = round($this->price*$this->number,2);

            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'datetime' => 'Время',
            'brigade_id' => 'Бригада',
            'status_id' => 'Статус',
            'stage_id' => 'Ступень',
            'work_id' => 'Выполеные работы',
            'note' => 'Описание',
            'money_picture' => 'Изображения',
            'project_id' => 'Проект',
            'kurs' => 'Курс',
            'is_paid' => 'Не оплаченые',
            'price'=>'Цена за единицу',
            'number'=>'Количество',
            'money' => 'Общая стоимость',
        ];
    }


    public function getBrigade()
    {
        return $this->hasOne(Brigade::className(), ['id' => 'brigade_id']);
    }

    public function getAvatar()
    {
        return $this->hasOne(Photo::className(), ['id' => 'money_picture']);
    }

    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    public function getStage()
    {
        return $this->hasOne(Stage::className(), ['id' => 'stage_id']);
    }

    public function getWork()
    {
        return $this->hasOne(Works::className(), ['id' => 'work_id']);
    }


//    public function getOperator()
//    {
//        return $this->hasOne(Users::className(), ['id' => 'operator_id']);
//    }

    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    public function getSuccess()
    {
        return $this->hasMany(WorkMoney::className(), ['work_id' => 'id'])->sum('money');

    }

}
