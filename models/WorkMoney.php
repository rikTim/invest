<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "work_money".
 *
 * @property integer $id
 * @property integer $work_id
 * @property string $money
 * @property string $text
 * @property string $datetime
 */
class WorkMoney extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material_money';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['work_id','money','investor_id'], 'required'],
            [['work_id','project_id','investor_id','type'], 'integer'],
            [['text'], 'string'],
            [['money','kurs'], 'string', 'max' => 45],
            [['datetime'], 'string', 'max' => 255],
            ['money','validateMoney','skipOnEmpty'=>false],
//            [['kurs'],'required','on'=>'update']

        ];
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->datetime = time();
                $this->type = 1;
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'work_id' => 'Work ID',
            'money' => 'Оплачено',
            'text' => 'Примечание',
            'datetime' => 'Дата',
            'kurs'=>'Курс',
        ];
    }

    public function getWork()
    {
        return $this->hasOne(Work::className(), ['id' => 'work_id']);
    }


    public function validateMoney()
    {
        $model = Work::find()->where(['id'=>$this->work_id])->one();

        $work_money = WorkMoney::find()->where(['work_id'=>$this->work_id])->all();
        $success = '';
        foreach($work_money as $money){
            $success += $money->money;
        }
        $minus = $model->money;
        $sum = $minus - $success;
        if($this->money > $sum) {
            $this->addError('money', ' Оплата не больше ' . $sum . '');
        }
    }
}
