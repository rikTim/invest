<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fabric".
 *
 * @property integer $id
 * @property integer $material_id
 * @property double $money
 * @property integer $money_picture
 * @property string $kurs
 * @property integer $project_id
 * @property integer $is_paid
 * @property double $price
 * @property double $number
 * @property integer $nomenclatura_id
 */
class FabricTest extends \yii\db\ActiveRecord
{

    public $project;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fabric';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['material_id', 'money', 'kurs', 'project_id', 'price', 'number', 'nomenclatura_id'], 'required','on'=>'create'],

            [[ 'project_id',  'nomenclatura_id'], 'required','on'=>'create_nomenklatura'],

            [['material_id', 'money_picture', 'project_id', 'is_paid', 'nomenclatura_id'], 'integer'],
            [['money', 'price', 'number'], 'number'],
            [['kurs'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 50],
            [['nomenclatura_id'], 'exist', 'skipOnError' => true, 'targetClass' => Nomenclatura::className(), 'targetAttribute' => ['nomenclatura_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'datetime' => 'Дата',
            'brigade' => 'Поставщик',
            'status_id' => 'Статус',
            'material_id' => 'Материал',
            'money_picture' => 'Фото',
            'kurs' => 'Курс',
            'project_id' => 'Проект',
            'price'=>'Цена за единицу',
            'number'=>'Количество',
            'money' => 'Общая стоимость',
        ];

    }

    public function getNomenclatura()
    {
        return $this->hasOne(Nomenclatura::className(), ['id' => 'nomenclatura_id']);
    }


    public function getMaterial()
    {
        return $this->hasOne(Material::className(), ['id' => 'material_id']);
    }


    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }
}
